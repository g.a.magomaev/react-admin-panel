import React, { FunctionComponent } from 'react';
import { useDispatch }              from 'react-redux';
import { Dispatch }                 from 'redux';
import styled                       from 'styled-components/macro';
import MainDrawerComponent          from '../../components/main-drawer/main-drawer.component';
import {
    LOGOUT,
    TAuthPageActions,
}                       from "../../redux/auth-page/auth-page.actions";
import { TReduxAction } from '../../redux/types/redux-action.interface';
import { TRouterLink }  from "../../types/router-link.type";
import MainRoutes       from './main-routes';


const Main: FunctionComponent = () => {
    const dispatch: Dispatch<TReduxAction<TAuthPageActions>> = useDispatch();

    const logoutHandler = () => {
        dispatch( LOGOUT() );
    };

    return (
        <Container>
            <MainDrawerComponent { ...{ links: drawerLinks, logoutHandler } }/>
            <Content>{ MainRoutes() }</Content>
        </Container>
    );
};

const drawerLinks: TRouterLink[] = [
    { id: 1, path: 'home', linkName: 'Home', icon: 'Home' },
    { id: 2, path: 'devices', linkName: 'Devices', icon: 'Devices' },
    { id: 3, path: 'settings', linkName: 'Settings', icon: 'Settings' },
    { id: 4, path: '', linkName: 'Exit', icon: 'ExitToApp' },
];

const Container = styled.div`
	display        : flex;
	flex-direction : row;
	width          : 100vw;
	height         : 100vh;
`;

const Content = styled.div`
	flex-grow  : 1;
	padding    : 16px 32px 0 32px;
	overflow-y : auto;
`;

export default Main;
