import React        from 'react';
import {
    Redirect,
    Route,
    Switch,
    useRouteMatch,
}                   from 'react-router-dom';
import DevicesPage  from '../devices/devices-page';
import HomePage     from '../home/home-page';
import SettingsPage from '../settings-page/settings-page';


const MainRoutes: () => JSX.Element = () => {
    const { url } = useRouteMatch<string>();
    return (
        <Switch>
            <Route path = { `${ url }/home` }
                   component = { HomePage }
            />
            <Route path = { `${ url }/devices` }
                   component = { DevicesPage }
            />
            <Route path = { `${ url }/settings` }
                   component = { SettingsPage }
            />

            <Redirect from = { `${ url }` }
                      to = { `${ url }/home` }
            />
        </Switch>
    );
};

export default MainRoutes;
