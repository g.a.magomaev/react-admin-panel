import { Box }                      from "@material-ui/core";
import React, { FunctionComponent } from 'react';
import HorizontalTabsComponent      from "../../components/horizontal-tabs/horizontal-tabs.component";
import { TRouterLink }              from "../../types/router-link.type";
import { SettingsPageRoutes }       from "./settings-page.routes";


const SettingsPage: FunctionComponent = () =>
    (
        <Box>
            <HorizontalTabsComponent links = { settingsPageTabsData }/>
            <Box>
                { SettingsPageRoutes() }
            </Box>
        </Box>
    );

const settingsPageTabsData: TRouterLink[] = [
    { id: 1, path: 'user-settings', linkName: 'User settings' },
    { id: 2, path: 'styles-settings', linkName: 'Styles settings' },
];

export default SettingsPage;
