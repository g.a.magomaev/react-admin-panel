import React from "react";
import { Switch, Route, Redirect, useRouteMatch } from "react-router-dom";
import { StylesSettingsTab } from "./tabs/style-settings/style_settings-tab";
import { UserSettingsTab } from "./tabs/user-settings/user_settings-tab";

export const SettingsPageRoutes: () => JSX.Element = () => {
  let { url } = useRouteMatch<string>();
  return (
    <Switch>
      <Route exact path={`${url}/user-settings`} component={UserSettingsTab} />
      <Route path={`${url}/styles-settings`} component={StylesSettingsTab} />
      <Redirect from={`${url}`} to={`${url}/user-settings`} />
    </Switch>
  );
};
