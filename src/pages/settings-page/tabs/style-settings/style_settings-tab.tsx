import {
    Checkbox,
    FormControlLabel,
}                              from "@material-ui/core";
import React, {
    FunctionComponent,
    useContext,
}                              from "react";
import styled                  from "styled-components/macro";
import {
    ThemeColorContext,
    ThemeToggleContext,
}                              from "../../../../context/theme.context";
import { THEME_COLOR_TYPE }    from "../../../../interfaces/constants";
import { TThemeColorContext }  from "../../../../hooks/theme/types/theme-color-context.type";
import { TThemeToggleContext } from "../../../../hooks/theme/types/theme-toggle-context.type";


const Container = styled.div``;

export const StylesSettingsTab: FunctionComponent = () => {
    const { themeType }   = useContext<TThemeColorContext>(ThemeColorContext);
    const { themeToggle } = useContext<TThemeToggleContext>(ThemeToggleContext);

    return (
        <Container>
            <FormControlLabel
                control = {
                    <Checkbox
                        color = "primary"
                        checked = { themeType === THEME_COLOR_TYPE.DARK }
                        onChange = { themeToggle }
                    />
                }
                label = "Dark mode"
                labelPlacement = "end"
            />
        </Container>
    );
};
