export type TDevicePageFooterProps = {
    [key: string]: () => void
};

