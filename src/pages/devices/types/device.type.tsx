import { IObjectKeys } from "../../../interfaces/objectKeys.interface";


export interface IDevice extends IObjectKeys {
    id: string;
    room_id: string;
    name: string;
    type: string;
    description: string;
}
