import { Button }                 from "@material-ui/core";
import React                      from "react";
import styled                     from "styled-components/macro";
import { TThemeColor }            from "../../../hooks/theme/types/theme-colors.type";
import { TDevicePageFooterProps } from "../types/device-page-footer-props";


const DevicePageFooter = (props: TDevicePageFooterProps) => {
    const { openCreateDeviceModal } = props;
    return (
        <Container>
            <Button
                color = "primary"
                variant = "contained"
                fullWidth = { true }
                type = "button"
                onClick = { openCreateDeviceModal }
            >
                Add device
            </Button>
        </Container>
    );
};

const Container = styled.div`
	position: sticky;
	bottom: 0;
	display: flex;
	flex-direction: column;
	align-items: center;
	padding: 16px 0;
	background-color: ${ ({ theme: { light01 } }: { theme: TThemeColor }) => light01 };

	button {
		width: 260px;
	}
`;

export default DevicePageFooter;
