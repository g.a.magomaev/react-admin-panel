import React, {
    FunctionComponent,
    useCallback,
    useEffect,
    useState,
}                        from "react";
import {
    connect,
    useDispatch,
    useSelector,
}                        from "react-redux";
import { Dispatch }      from "redux";
import styled            from "styled-components/macro";
import { Filter }        from "../../components/filter/filter.component";
import { IFilterOutput } from "../../components/filter/interfaces/filter-output.interface";
import SearchComponent   from "../../components/search/search.component";
import { SORT_ORDER }    from "../../components/table/enums/sort-order";
import TableComponent    from "../../components/table/table.component";
import { TSortOrder }    from "../../components/table/types/sort-order.type";
import { TTableCell }    from "../../components/table/types/table-cell.type";
import { MODAL_TYPE }    from "../../interfaces/constants";
import { TAppState }     from "../../redux/appState";
import { SHOW_MODAL }    from "../../redux/components/modal/modal.actions";
import {
    GET_DEVICES_FILTRATION,
    LOAD_DEVICES,
    SEARCH_DEVICES,
    SORT_DEVICES,
    TDevicesPageActions,
}                        from "../../redux/devices-page/devices-page.actions";
import { TReduxAction }  from "../../redux/types/redux-action.interface";
import { TSortProps }    from "../../types/sort-props.type";
import DevicePageFooter  from "./device-page-footer/device-page-footer.component";
import { IDevice }       from "./types/device.type";


const DevicesPage: FunctionComponent = () => {
    const dispatch: Dispatch<TReduxAction<TDevicesPageActions>> = useDispatch();

    const devices: IDevice[] = useSelector(
        ( state: TAppState ) => state.devicesPage.devices,
    );

    const [ order, setOrder ] = useState<TSortOrder>( SORT_ORDER.ASC );
    const [ orderBy, setOrderBy ] = useState<string>( '' );

    const loadDevicesList = useCallback(
        () => {
            dispatch( LOAD_DEVICES() );
        },
        [ dispatch ],
    );

    useEffect( loadDevicesList, [ loadDevicesList ] );

    const openCreateDeviceModal: () => void = () => {
        dispatch( SHOW_MODAL( { type: MODAL_TYPE.CREATE_DEVICE_MODAL } ) );
    };

    const openEditDeviceModal: ( deviceId: string ) => void = ( deviceId: string ) => {
        const fundedDevice: IDevice | undefined = devices.find( ( device: IDevice ) => device.id === deviceId );

        if ( fundedDevice ) {
            dispatch(
                SHOW_MODAL( { type: MODAL_TYPE.EDIT_DEVICE_MODAL, content: fundedDevice } ),
            );
        }
    };

    const sortDevices = ( { order, sortBy }: TSortProps ) => {
        dispatch( SORT_DEVICES( { sortBy: sortBy, order: order } ) );
        setOrder( order );
        setOrderBy( sortBy );
    };

    const searchDevices = ( value: string ) => {
        dispatch( SEARCH_DEVICES( value ) );
    };

    const filterDevices = ( value: IFilterOutput ) => {
        dispatch( GET_DEVICES_FILTRATION( value ) );
    };

    return (
        <Container>
            <HeadControls>
                <Filter
                    getValue = { filterDevices }
                    filterConfig = { [
                        { label: "Device type", filterBy: "type" },
                        { label: "Room id", filterBy: "room_id" },
                    ] }
                    items = { devices }
                />
                <SearchComponent emitter = { searchDevices }/>

            </HeadControls>
            { devices?.length ?
                <TableComponent head = { tableHead }
                                body = { devices }
                                rowClickFn = { openEditDeviceModal }
                                sortFn = { sortDevices }
                                updatedOrder = { order }
                                updatedOrderBy = { orderBy }
                /> : null }
            <DevicePageFooter { ...{ openCreateDeviceModal } }/>
        </Container>
    );
};

const tableHead: TTableCell[] = [
    { id: 'id', label: 'ID', width: '100px' },
    { id: 'room_id', label: 'Room id', width: '100px' },
    { id: 'name', label: 'Name' },
    { id: 'type', label: 'Type' },
    { id: 'description', label: 'Description' },
];

const Container = styled.div`
	display        : flex;
	flex-direction : column;
`;
const HeadControls = styled.div`
	display         : flex;
	flex-direction  : row;
	justify-content : space-between;
	padding-bottom  : 16px;

	& > * {
		width : 240px;
	}
`;

export default connect()( DevicesPage );
