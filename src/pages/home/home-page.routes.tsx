import React        from 'react';
import {
    Redirect,
    Route,
    Switch,
    useRouteMatch,
}                   from 'react-router-dom';
import HomeInfoTab  from './tabs/info-tab';
import { RoomsTab } from './tabs/rooms-tab';


const HomePageRoutes: () => JSX.Element = () => {
  const { url } = useRouteMatch<string>();

  return (
    <Switch>
      <Route path={`${url}/info`}  component={HomeInfoTab} />
      <Route exact path={`${url}/rooms`} component={RoomsTab} />
      <Redirect from={`${url}`} to={`${url}/info`} />
    </Switch>
  );
};

export default HomePageRoutes;
