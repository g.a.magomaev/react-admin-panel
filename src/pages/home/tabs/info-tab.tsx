import React, { FunctionComponent } from 'react';
import styled                       from 'styled-components';


const HomeInfoTab: FunctionComponent = () => ( <Container> Info </Container> );

const Container = styled.div`
	display        : flex;
	flex-direction : column;
	width          : 100%;
`;

export default HomeInfoTab;
