import { Box }                      from "@material-ui/core";
import React, { FunctionComponent } from 'react';
import HorizontalTabsComponent      from '../../components/horizontal-tabs/horizontal-tabs.component';
import { TRouterLink }              from "../../types/router-link.type";
import HomePageRoutes               from './home-page.routes';


const HomePage: FunctionComponent = () =>
    (
        <Box>
            <HorizontalTabsComponent links = { homePageTabsData }/>
            <Box>{ HomePageRoutes() }</Box>
        </Box>
    );

const homePageTabsData: TRouterLink[] = [
    { id: 1, path: 'info', linkName: 'Info' },
    { id: 2, path: 'rooms', linkName: 'Rooms' },
];

export default HomePage;
