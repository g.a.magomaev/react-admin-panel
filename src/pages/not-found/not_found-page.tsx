import React, { FunctionComponent } from "react";

export const NotFoundPage: FunctionComponent = () => {
  return (
    <div>
      <div>404</div>
      <div>Page not found</div>
    </div>
  );
};
