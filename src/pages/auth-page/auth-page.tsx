import {
    Button,
    TextField,
}                                   from '@material-ui/core';
import {
    FormikValues,
    useFormik,
}                                   from "formik";
import React, { FunctionComponent } from 'react';
import {
    connect,
    useDispatch,
}                                   from 'react-redux';
import { Dispatch }                 from 'redux';
import styled                       from "styled-components/macro";
import * as Yup                     from "yup";
import ErrorBoundary                from '../../components/error-boundary/error-boundary.component';
import { HTML_INPUT_TYPE }          from "../../enums/html-input-type.enum";
import {
    LOGIN,
    REGISTER,
} from "../../redux/auth-page/auth-page.actions";

import { TReduxAction }          from '../../redux/types/redux-action.interface';
import { RegisterLoginUserData } from "./types/register-login-user-data";
import { TThemeColor }           from "../../hooks/theme/types/theme-colors.type";
import { EmailValidationSchema } from "../../validation-schemas/email.validation-schema";
import { PasswordValidationSchema } from "../../validation-schemas/password.validation-schema";


const AuthPage: FunctionComponent = () => {
    const dispatch: Dispatch<TReduxAction<RegisterLoginUserData>> = useDispatch();

    const { getFieldProps, touched, errors, isValid, dirty, handleSubmit, values } = useFormik({
            initialValues:    { email: '', password: '' },
            validationSchema: Yup.object({
                email:    EmailValidationSchema,
                password: PasswordValidationSchema,
            }),
            onSubmit:         ({ email, password }: FormikValues) => {
                dispatch(LOGIN({ email, password }));
            },
        },
    )

    const registerHandler: () => void = () => {
        const { email, password } = values;
        dispatch(REGISTER({ email, password }));
    };


    return (
        <ErrorBoundary>
            <Container>
                <Card>
                    <CardTitle>Авторизация</CardTitle>
                    <CardForm onSubmit = { handleSubmit }>
                        <TextField
                            fullWidth
                            id = { HTML_INPUT_TYPE.EMAIL }
                            type = { HTML_INPUT_TYPE.EMAIL }
                            className = 'input-field'
                            label = 'Email'
                            placeholder = 'Введите email'
                            { ...getFieldProps(HTML_INPUT_TYPE.EMAIL) }
                            error = { touched.email && Boolean(errors.email) }
                            helperText = { touched.email && errors.email }
                            InputLabelProps = { {
                                shrink: true,
                            } }
                        />
                        <TextField
                            fullWidth
                            id = { HTML_INPUT_TYPE.PASSWORD }
                            type = { HTML_INPUT_TYPE.PASSWORD }
                            className = 'input-field'
                            label = 'Password'
                            placeholder = 'Введите password'
                            { ...getFieldProps(HTML_INPUT_TYPE.PASSWORD) }
                            error = { touched.password && Boolean(errors.password) }
                            helperText = { touched.password && errors.password }
                            InputLabelProps = { {
                                shrink: true,
                            } }
                        />
                        <CardFormButtons>
                            <Button color = "primary"
                                    variant = "contained"
                                    fullWidth = { true }
                                    type = "submit"
                                    disabled = { !isValid || !dirty }
                            >ВОЙТИ</Button>
                            <Button color = "primary"
                                    variant = "contained"
                                    fullWidth = { true }
                                    type = "button"
                                    disabled = { !isValid || !dirty }
                                    onClick = { registerHandler }
                            >РЕГИСТРАЦИЯ
                            </Button>
                        </CardFormButtons>
                    </CardForm>
                </Card>
            </Container>
        </ErrorBoundary>
    );
};


const Container = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	width: 100vw;
	height: 100vh;
`;

const Card = styled.div`
	padding: 16px;
	width: 340px;
	box-shadow: 0 0 8px 0 ${ ({ theme: { disabled24 } }: { theme: TThemeColor }) => disabled24 };
	background-color: ${ ({ theme: { light01 } }: { theme: TThemeColor }) => light01 };
`;

const CardTitle = styled.div`
	font-size: 24px;
	text-align: center;
	margin-bottom: 10px;
`;

const CardForm = styled.form`
	flex-direction: column;

	.input-field {
		height: 72px;
		margin-bottom: 10px;
	}
`;

const CardFormButtons = styled.div`
	display: flex;
	justify-content: space-around;

	& > *:first-child {
		margin-right: 8px;
	}
`;

export default connect()(AuthPage);
