export type RegisterLoginUserData = {
    email: string;
    password: string;
}
