export type TRegisterLoginResponse = {
    email: string,
    password: string,
}