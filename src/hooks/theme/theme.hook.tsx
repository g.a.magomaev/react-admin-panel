import {
    useEffect,
    useState,
}                     from "react";
import { TThemeHook } from "./types/theme-hook.type";


const THEME_NAME: string = "colorTheme";

export const useColorTheme: TThemeHook = () => {
    const [ state, setState ] = useState<string>("");

    const setColorTheme: (colorType: string) => void = (colorType: string) => {
        window.localStorage.setItem(THEME_NAME, `${ colorType }`);
        setState(colorType);
    };

    const getColorTheme: () => string = () => {
        return window.localStorage.getItem(THEME_NAME) || "light";
    };

    useEffect(() => {
        const localTheme: string = getColorTheme();
        if ( localTheme ) {
            setState(localTheme);
        }
    }, []);

    return [ state, setColorTheme ];
};
