import { TThemeHookReturns } from "./theme-hook-returns.type";


export type TThemeHook = {
    (): TThemeHookReturns;
};
