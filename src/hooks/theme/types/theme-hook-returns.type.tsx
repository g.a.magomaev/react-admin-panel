export type TThemeHookReturns = readonly [ string, (colorType: string) => void ];
