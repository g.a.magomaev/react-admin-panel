export type TThemeToggleContext = {
    themeToggle: () => void;
};
