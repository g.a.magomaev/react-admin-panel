import { TThemeColor } from "./theme-colors.type";


export type TTheme = {
    light: TThemeColor;
    dark: TThemeColor;
};
