import React, { FunctionComponent } from 'react';
import { BrowserRouter }            from 'react-router-dom';
import styled                       from 'styled-components/macro';
import AppRoutes         from './app-routes';
import DropDownComponent from "./components/dropdown/dropdown.component";

import { MainModal }          from './components/modal/main-modal.component';
import { Spinner }            from './components/spinner/spinner.component';
import { Toast }              from './components/toast/toast.component';
import { ThemeColorProvider } from './context/theme.context';
import { TThemeColor }        from './hooks/theme/types/theme-colors.type';


const App: FunctionComponent = () => (
    <ThemeColorProvider>
        <Container>
            <BrowserRouter>{ AppRoutes() }</BrowserRouter>

            <div id = "modal-root"/>
            <div id = "toast-root"/>
            <div id = "spinner-root"/>
            <div id = "dropdown-root"/>
            <MainModal/>
            <Toast/>
            <Spinner/>
            <DropDownComponent/>
        </Container>
    </ThemeColorProvider>
);

const Container = styled.div`
	* {
		box-sizing                    : border-box;
		font-family                   : 'Inter', sans-serif;

		//------------- Font styles --------------//
		-webkit-font-feature-settings : 'zero' 1;
		-moz-font-feature-settings    : 'zero' 1;
		font-feature-settings         : 'zero' 1;
	}

	background-color : ${ ( { theme }: { theme: TThemeColor } ) => theme.light01 };
`;

export default App;
