import React, {
    Dispatch,
    useEffect,
}                       from 'react';
import {
    useDispatch,
    useSelector,
}                       from 'react-redux';
import {
    Redirect,
    Route,
    Switch,
}                       from 'react-router-dom';
import AuthPage         from './pages/auth-page/auth-page';
import Main             from './pages/main/main';
import { TAppState }    from './redux/appState';
import {
    IS_AUTHORIZED,
    TAuthPageActions,
}                       from './redux/auth-page/auth-page.actions';
import { TReduxAction } from './redux/types/redux-action.interface';


const AppRoutes: () => JSX.Element = () => {
    const dispatch: Dispatch<TReduxAction<TAuthPageActions>> = useDispatch();
    const token: string                                      = useSelector<TAppState, string>((state) => state.authPage.userData.token);

    useEffect(() => {
        dispatch(IS_AUTHORIZED());
    }, [ dispatch ]);

    if ( token ) {
        return (
            <Switch>
                <Route path = "/admin"
                       component = { Main }
                />
                <Redirect from = "/"
                          to = "/admin"
                />
            </Switch>
        );
    }

    return (
        <Switch>
            <Route path = "/auth"
                   exact
                   component = { AuthPage }
            />
            <Redirect from = "/"
                      to = "/auth"
            />
        </Switch>
    );
};

export default AppRoutes;
