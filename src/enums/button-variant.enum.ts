export enum BUTTON_VARIANT {
    OUTLINED  = 'outlined',
    CONTAINED = 'contained',
}
