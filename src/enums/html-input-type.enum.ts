export enum HTML_INPUT_TYPE {
    EMAIL    = 'email',
    PASSWORD = 'password',
    TEXT     = 'text'
}
