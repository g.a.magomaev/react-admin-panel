import React, {
    Context,
    createContext,
    ReactNode,
} from "react";

import { ThemeProvider }       from "styled-components";
import { theme }            from "../assets/styles/theme";
import { useColorTheme }    from "../hooks/theme/theme.hook";
import { THEME_COLOR_TYPE } from "../interfaces/constants";
import { TThemeColorContext }  from "../hooks/theme/types/theme-color-context.type";
import { TThemeToggleContext } from "../hooks/theme/types/theme-toggle-context.type";


export const ThemeColorContext: Context<TThemeColorContext>   = createContext({ themeType: "" });
export const ThemeToggleContext: Context<TThemeToggleContext> = createContext({
    themeToggle: () => {
    },
});

export const ThemeColorProvider = ({ children }: { children: ReactNode }) => {
    const [ colorTheme, setColorTheme ] = useColorTheme();

    const toggle = () => {
        const currentTheme =
                  colorTheme === THEME_COLOR_TYPE.DARK ? THEME_COLOR_TYPE.LIGHT : THEME_COLOR_TYPE.DARK;

        setColorTheme(currentTheme);
    };

    return (
        <ThemeColorContext.Provider value = { { themeType: colorTheme } }>
            <ThemeToggleContext.Provider value = { { themeToggle: toggle } }>
                <ThemeProvider theme = { colorTheme === "dark" ? theme.dark : theme.light }>
                    { children }
                </ThemeProvider>
            </ThemeToggleContext.Provider>
        </ThemeColorContext.Provider>
    );
};
