import { TSortOrder }  from "../components/table/types/sort-order.type";
import { IObjectKeys } from "./objectKeys.interface";


export interface ISortProps extends IObjectKeys {
    sortBy: string,
    order: TSortOrder
}
