import { IObjectKeys } from "./objectKeys.interface";

export interface IFilterInputItem extends IObjectKeys {
  [key: string]: {
    name: string;
    children: string[];
  };
}
