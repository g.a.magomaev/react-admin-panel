// ---------- Theme Enums ---------- //
export enum THEME_COLOR_TYPE {
  LIGHT = "light",
  DARK = "dark",
}

// ---------- Button Enums ---------- //
export enum BUTTON_TYPE_STYLE {
  PRIMARY = "primary",
  SECONDARY = "secondary",
  TEXT = "text",
  DELETE = "delete",
}

// ---------- Search Enums ---------- //

export enum QUERY_TYPE {
  SEARCH = "search",
  FILTER = "filter",
  SORT = "sort",
}

// ---------- Modals Types Enums ---------- //

export enum MODAL_TYPE {
  CREATE_DEVICE_MODAL = "createDeviceModal",
  EDIT_DEVICE_MODAL = "editDeviceModal",
}

// ---------- Modals Actions Enums ---------- //

export enum MODAL_BUTTON_ACTION {
  CONFIRM = "confirm",
  CANCEL = "cancel",
  DELETE = "delete",
}

// ---------- Modals Footer Buttons Positions ---------- //

export enum MODAL_FOOTER_BTN_POSITION {
  START = "flex-start",
  END = "flex-end",
  CENTER = "center",
  AROUND = "space-around",
  BETWEEN = "space-between",
}

// ---------- HTTP Request Methods Enums ---------- //

export enum HTTP_REQUEST_METHOD {
  POST = "POST",
  GET = "GET",
  PUT = "PUT",
  DELETE = "DELETE",
}

// ---------- Application's Paths ---------- //

export enum APP_PATHS {
  REGISTER = '/auth/register',
  LOGIN = '/auth/login',
  DEVICES = "/devices",
  DEVICE = "/device",
  DEVICE_UPDATE = "/device",
  DEVICE_DELETE = "/device",
}
