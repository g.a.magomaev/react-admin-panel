import { ISortProps } from "../interfaces/sort-props.interface";


export type TRequestProps = {
    url: string;
    method?: string;
    body?: any;
    headers?: any;
    searchQuery?: string;
    queryType?: string;
    sortQuery?: ISortProps;
};
