import { TSortOrder } from "../components/table/types/sort-order.type";


export type TSortProps = { sortBy: string, order: TSortOrder }
