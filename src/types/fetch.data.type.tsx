export type TFetchData = {
    fullUrl: string;
    requestInit: RequestInit;
};
