import { TUser } from '../pages/auth-page/types/user.type';


export type TResponseMessage = {
    message: [ string ];
    statusCode: number;
    error?: string;
    data?: TUser;
};
