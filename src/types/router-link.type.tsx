import { TIconTypes } from "../components/icon/types/icon-types.type";


export type TRouterLink =
    {
        id: number,
        path: string,
        linkName: string,
        icon?: TIconTypes,
    }
