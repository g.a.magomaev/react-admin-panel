export type TButtonVariantType =
    'outlined'
    | 'contained';
