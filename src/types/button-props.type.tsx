import { PropTypes }          from "@material-ui/core";
import { TIconTypes }         from "../components/icon/types/icon-types.type";
import { TButtonVariantType } from "./button-variant.type";


export type TButtonProps = {
    name?: string;
    disabled?: boolean;
    onClickFn?: (...[ props ]: any) => void;
    style?: { [key: string]: string };
    action?: any;
    color?: PropTypes.Color;
    endIcon?: TIconTypes;
    startIcon?: TIconTypes;
    variant?: TButtonVariantType;
    canDisabled?: boolean;
};
