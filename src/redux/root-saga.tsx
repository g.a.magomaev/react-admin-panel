import { all } from "redux-saga/effects";
import { authPageSaga } from './auth-page/auth-page.saga';
import { devicesPageSaga } from "./devices-page/devices-page.saga";

export default function* rootSaga() {
  yield all([devicesPageSaga(), authPageSaga()]);
}
