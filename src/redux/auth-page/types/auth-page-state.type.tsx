import { TUser } from '../../../pages/auth-page/types/user.type';


export type TAuthPageState = {
    userData: TUser
};
