import {
    AnyAction,
    createReducer,
    Reducer,
}                         from "@reduxjs/toolkit";
import { TAuthPageState } from './types/auth-page-state.type';
import { TUser }          from '../../pages/auth-page/types/user.type';
import { SET_DATA_SESSION } from "./auth-page.actions";



const initialState: TAuthPageState = {
    userData: {
        id:    '',
        token: '',
    },
};

export const authPageReducers: Reducer<TAuthPageState, AnyAction> = createReducer(initialState, {
    [SET_DATA_SESSION.type]: (state, action) => {
        const { id, token }: TUser = action.payload;
        return { ...state, userData: { ...state.userData, id, token } };
    },
});
