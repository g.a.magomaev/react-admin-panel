import {
    ActionCreatorWithoutPayload,
    ActionCreatorWithPayload,
    createAction,
} from "@reduxjs/toolkit";

import { TUser }                  from '../../pages/auth-page/types/user.type';
import { TRegisterLoginResponse } from "../../pages/auth-page/types/register-response.type";


const moduleName: string = `DEVICES_PAGE`;

export const IS_AUTHORIZED: ActionCreatorWithoutPayload<string> = createAction(`${ moduleName }/IS_AUTHORIZED`);

export const SET_DATA_SESSION: ActionCreatorWithPayload<TUser, string> = createAction<TUser>(`${ moduleName }/SET_DATA_SESSION`);

export const REGISTER: ActionCreatorWithPayload<TRegisterLoginResponse, string> = createAction<TRegisterLoginResponse>(`${ moduleName }/REGISTER`);

export const LOGIN: ActionCreatorWithPayload<TRegisterLoginResponse, string> = createAction<TRegisterLoginResponse>(`${ moduleName }/LOGIN`);

export const LOGOUT: ActionCreatorWithoutPayload<string> = createAction(`${ moduleName }/LOGOUT`);

export type TAuthPageActions =
    void
    | TRegisterLoginResponse
    | string
    | TUser;
