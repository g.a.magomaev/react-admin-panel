import {
    all,
    fork,
    put,
    take,
    takeEvery,
}                           from 'redux-saga/effects';
import {
    APP_PATHS,
    HTTP_REQUEST_METHOD,
}                           from '../../interfaces/constants';
import { TRequestProps }    from '../../types/request-props.type';
import { TResponseMessage } from '../../types/response-message.type';
import { TUser }            from '../../pages/auth-page/types/user.type';
import { Utils }            from '../../utilities/utils';

import {
    HIDE_SPINNER,
    SHOW_SPINNER,
}                           from '../components/spinner/spinner.actions';
import { SHOW_TOAST }       from '../components/toast/toast.actions';
import {
    IS_AUTHORIZED,
    LOGIN,
    LOGOUT,
    REGISTER,
    SET_DATA_SESSION,
} from "./auth-page.actions";



const STORAGE_NAME = 'userData';

function* isAuthorized () {
    yield takeEvery(IS_AUTHORIZED.type, checkAuthorization);
}

function* checkAuthorization () {
    const data = JSON.parse(localStorage.getItem(STORAGE_NAME) || '{}');

    if ( data ) {
        yield put(SET_DATA_SESSION(data));
    }
}

function* register () {
    while (true) {
        const { payload: { email, password } } = yield take(REGISTER.type);
        yield fork(getCRUD, {
            url:    APP_PATHS.REGISTER,
            method: HTTP_REQUEST_METHOD.POST,
            body:   { email, password },
        });
    }
}

function* login () {
    while (true) {
        const {
                  payload: { email, password },
              } = yield take(LOGIN.type);
        yield fork(getCRUD, {
            url:    APP_PATHS.LOGIN,
            method: HTTP_REQUEST_METHOD.POST,
            body:   { email, password },
        });
    }
}

function* logout () {
    yield takeEvery(LOGOUT.type, removeDataSession);
}

function* getCRUD ({ url, method, body }: TRequestProps) {
    yield put(SHOW_SPINNER());

    const { statusCode, message, data }: TResponseMessage = yield Utils.getFetch({
        url,
        method,
        body,
    });

    if ( statusCode >= 200 && statusCode < 300 && data ) {
        yield fork(setDataSession, data);
    }

    yield put(
        SHOW_TOAST({
            message,
            severity: Utils.httpErrorRecognition(statusCode),
        }),
    );

    yield put(HIDE_SPINNER());
}

function* setDataSession ({ id, token }: TUser) {
    localStorage.setItem(STORAGE_NAME, JSON.stringify({ id, token }));
    yield put(SET_DATA_SESSION({ id, token }));
}

function* removeDataSession () {
    localStorage.removeItem(STORAGE_NAME);

    yield put(SET_DATA_SESSION({ id: '', token: '' }));
}

export function* authPageSaga () {
    yield all([ isAuthorized(), register(), login(), logout() ]);
}
