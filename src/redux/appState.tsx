import {
    CombinedState,
    combineReducers,
    Reducer,
}                            from "redux";
import { TAuthPageState }    from './auth-page/types/auth-page-state.type';
import { TDevicesPageState } from "./devices-page/types/devices-page-state.type";
import { TModalState }       from "./components/modal/types/modal-state.type";
import { TReduxAction }      from "./types/redux-action.interface";
import { authPageReducers } from './auth-page/auth-page.reducer';
import { dropdownReducers }    from "./components/dropdown/dropdown.reducers";
import { TDropdownState }      from "./components/dropdown/types/dropdown-state.type";
import { modalReducers }       from "./components/modal/modal.reducer";
import {
    spinnerReducers,
    TSpinnerState,
}                              from "./components/spinner/spinner.reducers";
import {
    toastReducers,
    TToastState,
}                              from "./components/toast/toast.reducers";
import { devicesPageReducers } from "./devices-page/devices-page.reducer";


export type TAppState = {
    authPage: TAuthPageState;
    devicesPage: TDevicesPageState;
    modal: TModalState;
    toast: TToastState;
    spinner: TSpinnerState;
    dropdown: TDropdownState;
};

export const appState: Reducer<CombinedState<TAppState>,
    TReduxAction<TAppState>> = combineReducers( {
    authPage:    authPageReducers,
    devicesPage: devicesPageReducers,
    modal:       modalReducers,
    toast:       toastReducers,
    spinner:     spinnerReducers,
    dropdown:    dropdownReducers,
} );
