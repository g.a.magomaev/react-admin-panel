import {
    all,
    fork,
    put,
    take,
    takeEvery,
}                           from "redux-saga/effects";
import { SORT_ORDER }       from "../../components/table/enums/sort-order";
import {
    APP_PATHS,
    HTTP_REQUEST_METHOD,
    QUERY_TYPE,
}                           from "../../interfaces/constants";
import { ISortProps }    from "../../interfaces/sort-props.interface";
import { IDevice }       from "../../pages/devices/types/device.type";
import { TRequestProps } from "../../types/request-props.type";
import { TResponseMessage } from "../../types/response-message.type";
import { Utils }            from "../../utilities/utils";
import { HIDE_MODAL }       from "../components/modal/modal.actions";
import {
    HIDE_SPINNER,
    SHOW_SPINNER,
}                           from "../components/spinner/spinner.actions";
import { SHOW_TOAST }       from "../components/toast/toast.actions";
import {
    CREATE_DEVICE,
    DELETE_DEVICE,
    GET_DEVICES_FILTRATION,
    LOAD_DEVICES,
    SAVE_DEVICES,
    SEARCH_DEVICES,
    SORT_DEVICES,
    UPDATE_DEVICE,
}                           from "./devices-page.actions";


function* getDevices () {
    yield takeEvery( LOAD_DEVICES.type, loadDevices, {} );
}

function* createDevice () {
    while ( true ) {
        const { payload: body } = yield take( CREATE_DEVICE.type );
        yield fork( getCRUD, {
            url:    APP_PATHS.DEVICE,
            method: HTTP_REQUEST_METHOD.POST,
            body,
        } );
    }
}

function* updateDevice () {
    while ( true ) {
        const { payload: body } = yield take( UPDATE_DEVICE.type );
        yield fork( getCRUD, {
            url:    APP_PATHS.DEVICE_UPDATE,
            method: HTTP_REQUEST_METHOD.PUT,
            body,
        } );
    }
}

function* deleteDevice () {
    while ( true ) {
        const { payload: { id } } = yield take( DELETE_DEVICE.type );
        yield fork( getCRUD, {
            url:    `${ APP_PATHS.DEVICE_DELETE }/${ id }`,
            method: HTTP_REQUEST_METHOD.DELETE,
        } );
    }
}

function* loadDevices ( {
    url = APP_PATHS.DEVICES,
    searchQuery = "",
    queryType = "",
    sortQuery = { order: SORT_ORDER.ASC, sortBy: '' },
} ) {
    yield put( SHOW_SPINNER() );
    const devices: IDevice[] = yield Utils.getFetch( {
        url,
        searchQuery,
        queryType,
        sortQuery,
    } );

    yield put( SAVE_DEVICES( devices ) );
    yield put( HIDE_SPINNER() );
    // yield put( SET_FILTER_ITEMS( Utils.getFilterData( [
    //         { label: "Device type", filterBy: "type" },
    //         { label: "Room id", filterBy: "room_id" },
    //     ], devices,
    //     ),
    //     ),
    // );
}

function* searchDevices () {
    while ( true ) {
        const { payload: searchQuery } = yield take( SEARCH_DEVICES.type );
        yield fork( loadDevices, {
            searchQuery,
            queryType: QUERY_TYPE.SEARCH,
        } );
    }
}

function* sortDevices () {
    while ( true ) {
        const { payload: sortQuery }: ISortProps = yield take( SORT_DEVICES.type );
        yield fork( loadDevices, {
            sortQuery,
            queryType: QUERY_TYPE.SORT,
        } );
    }
}

function* devicesFiltration () {
    while ( true ) {
        const { payload } = yield take( GET_DEVICES_FILTRATION.type );
        yield fork( loadDevices, {
            searchQuery: JSON.stringify( payload ),
            queryType:   QUERY_TYPE.FILTER,
        } );
    }
}

function* getCRUD ( { url, method, body }: TRequestProps ) {
    yield put( SHOW_SPINNER() );
    const response: TResponseMessage = yield Utils.getFetch( {
        url,
        method,
        body,
    } );

    yield put( HIDE_SPINNER() );
    yield put(
        SHOW_TOAST( {
            message:  response.message,
            severity: Utils.httpErrorRecognition( response.statusCode ),
        } ),
    );

    if ( response.statusCode === 200 || response.statusCode === 201 ) {
        yield put( HIDE_MODAL() );
        yield put( LOAD_DEVICES() );
    }
}

export function* devicesPageSaga () {
    yield all( [
        getDevices(),
        sortDevices(),
        searchDevices(),
        devicesFiltration(),
        createDevice(),
        updateDevice(),
        deleteDevice(),
    ] );
}
