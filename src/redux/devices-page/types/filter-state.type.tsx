import { IFilterOutput }    from "../../../components/filter/interfaces/filter-output.interface";
import { IFilterInputItem } from "../../../interfaces/input-filter-item.interface";


export type TFilterState = {
    data: IFilterInputItem;
    filteredItems: IFilterOutput;
    isFiltered: boolean;
};
