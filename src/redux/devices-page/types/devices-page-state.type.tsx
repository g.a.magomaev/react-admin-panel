import { IDevice }      from "../../../pages/devices/types/device.type";
import { TFilterState } from "./filter-state.type";


export type TDevicesPageState = {
    devices: IDevice[];
    filter: TFilterState;
};
