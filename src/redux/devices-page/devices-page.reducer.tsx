import { createReducer } from "@reduxjs/toolkit";
import { IFilterInputItem }  from "../../interfaces/input-filter-item.interface";
import { IDevice }                        from "../../pages/devices/types/device.type";
import { TDevicesPageState }              from "./types/devices-page-state.type";
import { SAVE_DEVICES, SET_FILTER_ITEMS } from "./devices-page.actions";

const initialState: TDevicesPageState = {
  devices: [],
  filter: { data: {}, filteredItems: {}, isFiltered: false },
};

export const devicesPageReducers = createReducer(initialState, {
  [SET_FILTER_ITEMS.type]: (state, action) => {
    const data: IFilterInputItem = action.payload;
    return { ...state, filter: { ...state.filter, data } };
  },
  [SAVE_DEVICES.type]: (state, action) => {
    const devices: IDevice[] = action.payload;
    return { ...state, devices, loading: false };
  },
  // startLoading: (state) => ({ ...state, loading: true }),
  // setFilteredItems: (state, action) => {
  //   const filteredItems: IFilterOutput = action.payload;
  //   return {
  //     ...state,
  //     filter: { ...state.filter, filteredItems, isFiltered: true },
  //   };
  // },
});
