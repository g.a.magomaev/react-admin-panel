import {
    ActionCreatorWithoutPayload,
    ActionCreatorWithPayload,
    createAction,
}                           from "@reduxjs/toolkit";
import { IFilterOutput }    from "../../components/filter/interfaces/filter-output.interface";
import { IFilterInputItem } from "../../interfaces/input-filter-item.interface";
import { ISortProps }       from "../../interfaces/sort-props.interface";
import { IDevice }          from "../../pages/devices/types/device.type";
import { TShowModalAction } from "../components/modal/modal.actions";


const moduleName: string = `DEVICES_PAGE`;

export const LOAD_DEVICES: ActionCreatorWithoutPayload<string> = createAction( `${ moduleName }/LOAD_DEVICES` );

export const SAVE_DEVICES: ActionCreatorWithPayload<IDevice[], string> = createAction<IDevice[]>(
    `${ moduleName }/SAVE_DEVICES`,
);

export const SET_FILTER_ITEMS: ActionCreatorWithPayload<IFilterInputItem, string> = createAction<IFilterInputItem>(
    `${ moduleName }/SET_FILTER_ITEMS`,
);

export const GET_DEVICES_FILTRATION: ActionCreatorWithPayload<IFilterOutput, string> = createAction<IFilterOutput>(
    `${ moduleName }/DEVICES_FILTRATION`,
);

export const SORT_DEVICES: ActionCreatorWithPayload<ISortProps, string> = createAction<ISortProps>(
    `${ moduleName }/SORT_DEVICES`,
);

export const SEARCH_DEVICES: ActionCreatorWithPayload<string, string> = createAction<string>(
    `${ moduleName }/SEARCH_DEVICES`,
);

export const CREATE_DEVICE: ActionCreatorWithPayload<IDevice, string> = createAction<IDevice>(
    `${ moduleName }/CREATE_DEVICE`,
);
export const UPDATE_DEVICE: ActionCreatorWithPayload<IDevice, string> = createAction<IDevice>(
    `${ moduleName }/UPDATE_DEVICE`,
);
export const DELETE_DEVICE: ActionCreatorWithPayload<IDevice, string> = createAction<IDevice>(
    `${ moduleName }/DELETE_DEVICE`,
);

export type TDevicesPageActions =
    void
    | IDevice
    | string
    | IFilterOutput
    | ISortProps
    | TShowModalAction;
