export type TReduxAction<P> = {
    payload: P;
    type: string;
}
