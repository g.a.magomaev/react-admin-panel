import {
    createReducer,
    Reducer,
}                       from "@reduxjs/toolkit";
import { TModalState }  from "./types/modal-state.type";
import { TReduxAction } from "../../types/redux-action.interface";
import {
    HIDE_MODAL,
    SET_MODAL_DATA,
    SHOW_MODAL,
}                       from "./modal.actions";


const initialState: TModalState = {
    show:                  false,
    title:                 "",
    footer:                { buttonsProps: [], buttonsPosition: "", disabledBtn: false },
    type:                  null,
    content:               null,
    footerButtonsDisabled: true,
};
export const modalReducers: Reducer<TModalState,
    TReduxAction<TModalState>> = createReducer(initialState, {
    [SHOW_MODAL.type]:     (state: TModalState, action: TReduxAction<TModalState>) => ({
        ...state,
        ...action.payload,
        show: true,
    }),
    [SET_MODAL_DATA.type]: (state: TModalState, action) => ({
        ...state,
        ...action.payload,
    }),
    [HIDE_MODAL.type]:     (state: TModalState) => ({
        ...state,
        show:                  false,
        type:                  null,
        content:               null,
        footerButtonsDisabled: true,
    }),
});
