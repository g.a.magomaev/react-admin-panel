import { createAction } from "@reduxjs/toolkit";
import { IDevice }      from "../../../pages/devices/types/device.type";


export type TShowModalAction = { type: string; content?: any };

export const SHOW_MODAL     = createAction<TShowModalAction>(`SHOW_MODAL`);
export const HIDE_MODAL     = createAction(`HIDE_MODAL`);
export const SET_MODAL_DATA = createAction<{ content: IDevice, footerButtonsDisabled: boolean }>(`SET_MODAL_DATA`);
