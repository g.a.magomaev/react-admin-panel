import { TModalFooterProps } from "../../../../components/modal/modal-footer/types/modal-footer-props.type";
import { TModalTypes } from "../../../../components/modals/types/modals-types.type";
import { IDevice }     from "../../../../pages/devices/types/device.type";


export type TModalState = {
    show: boolean;
    title: string;
    type: TModalTypes | null;
    footer: TModalFooterProps;
    content: IDevice | null;
    footerButtonsDisabled?: boolean;
};
