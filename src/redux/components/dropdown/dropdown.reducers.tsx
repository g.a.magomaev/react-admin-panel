import { createReducer }  from "@reduxjs/toolkit";
import { TDropdownProps } from "../../../components/dropdown/types/dropdown-props.type";
import {
    HIDE_DROPDOWN,
    SHOW_DROPDOWN,
}                         from "./dropdown.actions";
import { TDropdownState } from "./types/dropdown-state.type";


const initialState: TDropdownState = {
    show:          false,
    element:       null,
    currentTarget: null,
};

export const dropdownReducers = createReducer( initialState, {
    [SHOW_DROPDOWN.type]: ( state, action ) => {
        const data: TDropdownProps = action.payload;
        return { ...state, show: true, ...data };
    },
    [HIDE_DROPDOWN.type]: ( state ) => ( {
        ...state,
        show:         false,
        element:      null,
        currentTarget: null,
    } ),
} );
