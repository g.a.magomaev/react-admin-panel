import {
    ActionCreatorWithoutPayload,
    ActionCreatorWithPayload,
    createAction,
}                         from "@reduxjs/toolkit";
import { TDropdownProps } from "../../../components/dropdown/types/dropdown-props.type";


export const SHOW_DROPDOWN: ActionCreatorWithPayload<TDropdownProps> = createAction<TDropdownProps>(
    "SHOW_DROPDOWN",
);
export const HIDE_DROPDOWN: ActionCreatorWithoutPayload<string> = createAction(
    "HIDE_DROPDOWN",
);
