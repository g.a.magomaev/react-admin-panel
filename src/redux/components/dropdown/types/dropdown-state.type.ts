export type TDropdownState = {
    show: boolean,
    currentTarget: EventTarget | null,
    element: JSX.Element | null,
}
