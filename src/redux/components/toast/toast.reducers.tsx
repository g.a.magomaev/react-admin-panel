import { createReducer } from "@reduxjs/toolkit";
import { TReduxAction }  from "../../types/redux-action.interface";
import { SHOW_TOAST }    from "./toast.actions";


export type TToastState = {
    message: string[];
    severity: string;
};

const initialState: TToastState = {
    message:  [],
    severity: "",
};

export const toastReducers = createReducer(initialState, {
    [SHOW_TOAST.type]: (
                           state: TToastState,
                           action: TReduxAction<TToastState>,
                       ) => ({
        ...state,
        ...action.payload,
    }),
});
