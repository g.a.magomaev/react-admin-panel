import { ActionCreatorWithPayload, createAction } from "@reduxjs/toolkit";
import { TToastState } from "./toast.reducers";

export const SHOW_TOAST: ActionCreatorWithPayload<
  TToastState,
  string
> = createAction<TToastState>("SHOW_TOAST");
