import { createReducer } from "@reduxjs/toolkit";
import { HIDE_SPINNER, SHOW_SPINNER } from "./spinner.actions";

export type TSpinnerState = {
  show: boolean;
};

const initialState: TSpinnerState = {
  show: false,
};

export const spinnerReducers = createReducer(initialState, {
  [SHOW_SPINNER.type]: (state: TSpinnerState) => ({
    ...state,
    show: true,
  }),
  [HIDE_SPINNER.type]: (state: TSpinnerState) => ({
    ...state,
    show: false,
  }),
});
