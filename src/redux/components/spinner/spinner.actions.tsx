import { ActionCreatorWithoutPayload, createAction } from "@reduxjs/toolkit";

export const SHOW_SPINNER: ActionCreatorWithoutPayload<"SHOW_SPINNER"> = createAction(
  "SHOW_SPINNER"
);
export const HIDE_SPINNER: ActionCreatorWithoutPayload<"HIDE_SPINNER"> = createAction(
  "HIDE_SPINNER"
);
