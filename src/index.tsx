import './wdyr';

import 'fontsource-roboto';

import React, { StrictMode }   from 'react';
import ReactDOM                from 'react-dom';
import { Provider }            from 'react-redux';
import {
    applyMiddleware,
    compose,
    createStore,
}                              from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware    from 'redux-saga';
import App                     from './App';
import './index.scss';
import { appState }            from './redux/appState';
import rootSaga                from './redux/root-saga';
import * as serviceWorker      from './serviceWorker';


const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    appState,
    compose( applyMiddleware( sagaMiddleware ), composeWithDevTools() ),
);

sagaMiddleware.run( rootSaga );

const app: JSX.Element = (
    <Provider store = { store }>
        <StrictMode>
            <App/>
        </StrictMode>
    </Provider>
);

ReactDOM.render( app, document.getElementById( 'root' ) );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
