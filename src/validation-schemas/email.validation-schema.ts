import { MessageParams } from "yup/lib/types";
import * as Yup from "yup";

export const EmailValidationSchema = Yup
.string()
.email(({ value }: MessageParams) => `${ value } is invalid email address`)
.required("Required")
.trim()
