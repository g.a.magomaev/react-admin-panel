import * as Yup from "yup";
import { MessageParams } from "yup/lib/types";

export const PasswordValidationSchema = Yup
.string()
.min(6, ({ value }: MessageParams) => `Your password length is ${ value.toString().length > 1 ? value.toString().length + ' symbols' : value.toString().length + ' symbol' }, must be more than six symbols`)
.required("Required")
.trim()
