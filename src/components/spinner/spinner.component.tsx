import React, { useEffect }  from "react";
import ReactDOM              from "react-dom";
import { useSelector }       from "react-redux";
import styled, { keyframes } from "styled-components/macro";
import { TAppState }         from "../../redux/appState";
import { TSpinnerState } from "../../redux/components/spinner/spinner.reducers";
import { TThemeColor }   from "../../hooks/theme/types/theme-colors.type";


export const Spinner: () => JSX.Element = () => {
    const wrapperElement: HTMLDivElement = document.createElement("div")!;

    const spinnerState: TSpinnerState = useSelector(
        (state: TAppState) => state.spinner,
    );

    useEffect(() => {
        const spinnerRoot: HTMLElement = document.getElementById("spinner-root")!;

        spinnerRoot.append(wrapperElement);
        return () => {
            spinnerRoot.removeChild(wrapperElement);
        };
    }, [ spinnerState.show, wrapperElement ]);

    const getSpinnerTemplate: () => JSX.Element = () => {
        return (
            <Container>
                <SpinnerBody viewBox = "0 0 50 50">
                    <Circle cx = "25"
                            cy = "25"
                            r = "20"
                            fill = "none"
                            stroke-width = "5"
                    />
                </SpinnerBody>
            </Container>
        );
    };
    return spinnerState.show ? (
        ReactDOM.createPortal(getSpinnerTemplate(), wrapperElement)
    ) : (
        <></>
    );
};

const Container = styled.div`
	position: absolute;
	top: 50%;
	left: 50%;
	display: flex;
	align-items: center;
	justify-content: center;
	z-index: 9999;
`;

const rotate = keyframes`
	100% {
		transform: rotate(360deg);
	}
`;

const dash = keyframes`
	0% {
		stroke-dasharray: 1, 150;
		stroke-dashoffset: 0;
	}
	50% {
		stroke-dasharray: 90, 150;
		stroke-dashoffset: -35;
	}
	100% {
		stroke-dasharray: 90, 150;
		stroke-dashoffset: -124;
	}
`;

const SpinnerBody = styled.svg`
	animation: ${ rotate } 2s linear infinite;
	width: 50px;
	height: 50px;
`;

const Circle = styled.circle`
	stroke: ${ ({ theme }: { theme: TThemeColor }) => theme.accent_blue };
	stroke-linecap: round;
	animation: ${ dash } 1.5s ease-in-out infinite;
`;
