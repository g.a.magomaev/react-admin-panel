export type TTableCell = {
    id: string;
    label: string;
    width?: string;
}
