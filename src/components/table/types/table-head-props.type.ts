import { TSortOrder } from "./sort-order.type";
import { TTableCell } from "./table-cell.type";


export type TTableHeadProps = {
    cells: TTableCell[];
    sortFn: ( headCellId: string ) => void;
    order: TSortOrder;
    orderBy: string;
}
