import { TSortProps } from "../../../types/sort-props.type";
import { TSortOrder } from "./sort-order.type";
import { TTableCell } from "./table-cell.type";


export type TTableProps = {
    head: TTableCell[],
    body: { [key: string]: string }[],
    rowClickFn?: ( rowId: string ) => void,
    updatedOrder?: TSortOrder,
    updatedOrderBy?: string,
    sortFn?: ( { sortBy, order }: TSortProps ) => void,
}
