import { TTableRow } from "./table-row.type";


export type TTableBodyProps = {
    rows: TTableRow[];
    rowClickFn?: ( rowId: string ) => void;
    selectedColumn?: string;
}
