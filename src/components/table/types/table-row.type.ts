import { TTableCell } from "./table-cell.type";


export type TTableRow = {
    id: string;
    cells: TTableCell[];
}
