import {
    TableBody,
    TableCell,
    TableRow,
    Theme,
    withStyles,
}                          from "@material-ui/core";
import React               from "react";
import { TTableBodyProps } from "../types/table-body-props.type";
import { TTableCell }      from "../types/table-cell.type";
import { TTableRow }       from "../types/table-row.type";


const TableBodyComponent: ( { rows, rowClickFn, selectedColumn }: TTableBodyProps ) => JSX.Element =
    ( { rows, rowClickFn, selectedColumn }: TTableBodyProps ) =>
        (
            <TableBody>
                { rows.map( ( row: TTableRow ) =>
                    <Row key = { row.id }
                         onClick = { rowClickFn ? () => rowClickFn( row.id ) : () => {
                         } }
                    >
                        { row.cells.map( ( cell: TTableCell, cellIndex: number ) =>
                            <Cell
                                key = { cell.id }
                                align = { cellIndex === 0 || cellIndex === 1 ? "center" : "left" }
                                padding = 'none'
                                variant = 'body'
                                { ...{
                                    width:  cell.width,
                                    active: selectedColumn === cell.id ? 'active' : 'notActive',
                                } }
                            >
                                { cell.label }

                            </Cell>,
                        ) }
                    </Row>,
                ) }
            </TableBody>
        );

const Row = withStyles( ( { palette: { grey }, transitions }: Theme ) => ( {
    root: {
        cursor:     'pointer',
        transition: transitions.create( [ 'background-color' ], { duration: 200, easing: 'linear' } ),

        '&:nth-child(2n-1)': {
            backgroundColor: grey["50"],
        },

        '&:hover': {
            backgroundColor: grey["200"],
        },
    },

} ) )
( TableRow );

const Cell = withStyles( ( { palette: { grey }, transitions }: Theme ) => ( {
    root: {
        width:        ( { width = 'auto' }: { width: string | undefined } ) => width,
        color:        ( { active }: any ) => active === 'active' ? grey["800"] : grey["600"],
        borderBottom: '0',
        padding:      '0 12px',
        fontSize:     '16px',
        transition:   transitions.create( [ 'color' ], { duration: 200, easing: 'linear' } ),

        '&:last-child': {
            padding: '0 12px',

        },
    },
} ) )( TableCell );

export default TableBodyComponent;
