import {
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel,
    Theme,
    withStyles,
}                          from "@material-ui/core";
import { ArrowDropDown }   from "@material-ui/icons";
import React               from "react";
import { SORT_ORDER }      from "../enums/sort-order";
import { TTableCell }      from "../types/table-cell.type";
import { TTableHeadProps } from "../types/table-head-props.type";


const TableHeadComponent: ( { cells, sortFn, order, orderBy }: TTableHeadProps ) => JSX.Element =
    ( { cells, sortFn, order, orderBy }: TTableHeadProps ) => {

        return (
            <TableHead>
                <TableRow>

                    {
                        cells.map( ( cell: TTableCell, cellIndex: number ) => (
                                <Cell
                                    key = { cell.id }
                                    align = { cellIndex === 0 || cellIndex === 1 ? "center" : "left" }
                                    padding = 'none'
                                    variant = 'head'
                                    sortDirection = { orderBy === cell.id ? order : false }
                                    style = { { width: cell.width } }
                                >
                                    <Label
                                        active = { orderBy === cell.id }
                                        direction = { orderBy === cell.id ? order : SORT_ORDER.ASC }
                                        onClick = { () => sortFn( cell.id ) }
                                        IconComponent = { ArrowDropDown }
                                        hideSortIcon = { orderBy !== cell.id }
                                    >
                                        { cell.label }
                                    </Label>
                                </Cell>
                            ),
                        )
                    }

                </TableRow>
            </TableHead> );
    };

const Cell = withStyles( {
    root: {
        borderBottom: '0',
        fontSize:     '14px',
    },
} )( TableCell );

const Label = withStyles( ( { palette: { grey }, transitions }: Theme ) => ( {
    root:   {
        position:   'relative',
        padding:    '0 12px',
        color:      grey["600"],
        transition: transitions.create( [ 'color' ], { duration: 200, easing: 'linear' } ),

        '&:hover': {
            color: grey["800"],
        },
    },
    icon:   {
        position: 'absolute',
        right:    '-12px',
    },
    active: {
        color: grey["800"],
    },
} ) )( TableSortLabel );

export default TableHeadComponent;
