import {
    Table,
    TableContainer,
}                         from "@material-ui/core";
import React, {
    useEffect,
    useState,
}                         from "react";
import { SORT_ORDER }     from "./enums/sort-order";
import TableBodyComponent from "./table-body/table-body.component";
import TableHeadComponent from "./table-head/table-head.component";
import { TSortOrder }     from "./types/sort-order.type";
import { TTableCell }     from "./types/table-cell.type";
import { TTableProps }    from "./types/table-data.type";
import { TTableRow }      from "./types/table-row.type";


const TableComponent: ( { head, body, rowClickFn, sortFn, updatedOrder, updatedOrderBy }: TTableProps ) => JSX.Element =
    ( { head: headCells, body: entities, rowClickFn, sortFn, updatedOrder, updatedOrderBy }: TTableProps ) => {
        const [ order, setOrder ] = useState<TSortOrder>( SORT_ORDER.ASC );
        const [ orderBy, setOrderBy ] = useState<string>( '' );
        const [ selectedColumn, setSelectedColumn ] = useState<string>( '' );

        const getSort = ( headCellId: string ) => {
            const updateOrder: TSortOrder = order === SORT_ORDER.ASC ? SORT_ORDER.DESC : SORT_ORDER.ASC;
            setOrder( updateOrder );
            if ( orderBy !== headCellId ) {
                setOrderBy( headCellId );
                setSelectedColumn( headCellId );
            }
            if ( sortFn ) {
                sortFn( { order: updateOrder, sortBy: headCellId } );
            }
        };

        useEffect( () => {
            setOrder( updatedOrder || SORT_ORDER.ASC );
            setOrderBy( updatedOrderBy || headCells[0].id );
            setSelectedColumn( updatedOrderBy || headCells[0].id );
        }, [ headCells, updatedOrder, updatedOrderBy ] );

        const getTableBodyRows = () => {
            let rows: TTableRow[] = [];
            entities.forEach( ( entity: { [key: string]: string } ) => {
                    let cells: TTableCell[] = [];
                    headCells.forEach( ( headCell: TTableCell ) => {
                            let cell: TTableCell = { id: headCell.id, label: entity[headCell.id] };
                            if ( headCell.width ) {
                                cell.width = headCell.width;
                            }
                            cells.push( cell );
                        },
                    );
                    rows.push( { id: entity.id, cells } );
                },
            );
            return rows;
        };

        return (
            <TableContainer>
                <Table>
                    <TableHeadComponent
                        sortFn = { getSort }
                        { ...{ cells: headCells, order, orderBy } }
                    />
                    <TableBodyComponent { ...{ rows: getTableBodyRows(), rowClickFn, selectedColumn } }/>
                </Table>
            </TableContainer> );
    };

export default TableComponent;
