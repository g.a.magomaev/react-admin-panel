import { TRouterLink } from "../../../types/router-link.type";


export type TMainDrawerProps = {
    links: TRouterLink[];
    logoutHandler: () => void;
}
