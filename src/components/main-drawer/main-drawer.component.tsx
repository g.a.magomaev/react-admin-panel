import {
    createStyles,
    Divider,
    Drawer,
    IconButton,
    makeStyles,
    Tab,
    Tabs,
    Theme,
}                           from "@material-ui/core";
import {
    ChevronLeft,
    ChevronRight,
}                           from "@material-ui/icons";
import clsx                 from "clsx";
import React, {
    ChangeEvent,
    FunctionComponent,
    useState,
}                           from "react";
import {
    useHistory,
    useRouteMatch,
}                      from "react-router-dom";
import { TRouterLink } from "../../types/router-link.type";
import IconComponent   from "../icon/icon.component";
import { TMainDrawerProps } from "./types/main-drawer-props.type";


const MainDrawerComponent: FunctionComponent<TMainDrawerProps> = ( {
    links,
    logoutHandler,
} ) => {
    const [ value, setValue ] = useState<number>( 1 );
    const { url } = useRouteMatch();

    const [ open, setOpen ] = useState( false );
    const { drawer, drawerClose, drawerOpen, drawerHeader, tabs, tab, exitTab, tabHover, indicator, selected } = useStyles();

    const handleDrawer = () => {
        setOpen( !open );
    };

    const history = useHistory();

    const handleChange = ( event: ChangeEvent<{}>, newValue: number ) => {

        if ( newValue === 4 ) {
            logoutHandler();
        }

        setValue( newValue );
        const linkObj: TRouterLink | undefined = links.find( ( link: TRouterLink ) => link.id === newValue );
        if ( linkObj ) {
            history.push( `${ url }/${ linkObj.path }` );
        }
    };

    return (
        <Drawer
            variant = "permanent"
            className = { clsx( drawer, {
                [drawerOpen]:  open,
                [drawerClose]: !open,
            } ) }
            classes = { {
                paper: clsx( {
                    [drawerOpen]:  open,
                    [drawerClose]: !open,
                } ),
            } }
        >
            <div className = { drawerHeader }>
                <IconButton onClick = { handleDrawer }
                            style = { { color: 'white' } }
                >
                    { open ? <ChevronLeft/> : <ChevronRight/> }
                </IconButton>
            </div>
            <Divider style = { { backgroundColor: 'grey' } }/>
            <Tabs
                value = { value }
                orientation = "vertical"
                indicatorColor = "primary"
                textColor = "primary"
                onChange = { handleChange }
                className = { tabs }
                classes = { { indicator } }
            >
                { links?.length && links.map( ( { id, linkName, icon }: TRouterLink ) =>
                    <Tab
                        key = { id }
                        value = { id }
                        label = { linkName }
                        icon = { icon && <IconComponent iconType = { icon }/> }
                        className = {
                            clsx( tab, {
                                [exitTab]:  id === 4,
                                [tabHover]: id !== value,
                            } )
                        }
                        classes = { { selected } }
                    />,
                ) }
            </Tabs>
        </Drawer>
    );
};

const useStyles = makeStyles(
    ( { spacing, palette }: Theme ) => {
        const drawerWidth = 240;
        return createStyles( {
                drawer:       {
                    width:      drawerWidth,
                    flexShrink: 0,
                },
                drawerHeader: {
                    display:        'flex',
                    alignItems:     'center',
                    justifyContent: 'flex-end',

                    '& > button': {
                        padding: '18px',
                    },
                },
                drawerOpen:   {
                    width:           drawerWidth,
                    backgroundColor: palette.common.black,
                    transition:      'width 0.2s linear',
                },
                drawerClose:  {
                    transition:      'width 0.2s linear',
                    overflowX:       'hidden',
                    width:           spacing( 8 ),
                    backgroundColor: palette.common.black,
                },
                tabs:         {
                    height:     'inherit',
                    paddingTop: '16px',
                    '& > *':    {
                        height:  'inherit',
                        '& > *': {
                            height: 'inherit',
                        },
                    },
                },
                tab:          {
                    width:     '100%',
                    minHeight: '0',
                    padding:   '8px 8px 8px 20px',
                    color:     palette.common.white,

                    '& > *':      {
                        flexDirection:  'row',
                        alignItems:     'flex-start',
                        justifyContent: 'end',

                        '& > svg': {
                            marginRight: '20px',
                        },
                    },
                    '&$selected': {
                        color: `${ palette.info.main }`,
                    },
                },
                exitTab:      {
                    margin:  'auto 0 0 0',
                    '& > *': {
                        '& > svg': {
                            transform: 'rotate(180deg)',
                        },
                    },
                },
                tabHover:     {
                    '&:hover': {
                        color:      palette.warning.light,
                        transition: 'color 0.3s linear',
                    },
                },
                selected:     {},
                indicator:    {
                    backgroundColor: palette.info.main,
                    width:           '4px',
                },
            },
        );
    },
);

export default MainDrawerComponent;
