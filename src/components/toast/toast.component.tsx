import { ClickAwayListener, Fade, Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { useSelector } from "react-redux";
import { TAppState } from "../../redux/appState";
import { TToastState } from "../../redux/components/toast/toast.reducers";

type TToastProps = {
  severity?: string;
  message?: string;
  stateSelector?: any;
};

function Alert(props: any) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export const Toast = ({ severity, message }: TToastProps) => {
  const [show, setToastState] = useState<boolean>(false);

  const wrapperElement: HTMLDivElement = document.createElement("div");

  const toastState: TToastState = useSelector(
    (state: TAppState) => state.toast
  );

  useEffect(() => {
    if (severity && message) {
      setToastState(true);
    }
  }, [severity, message]);

  useEffect(() => {
    const { severity, message } = toastState;

    if (severity && message) {
      setToastState(true);
    }
  }, [toastState]);

  useEffect(() => {
    const modalRoot: HTMLElement = document.getElementById("toast-root")!;

    modalRoot.append(wrapperElement);
    return () => {
      modalRoot.removeChild(wrapperElement);
    };
  }, [show, wrapperElement]);

  const hideToast = () => {
    setToastState(false);
  };

  const toastTemplate: () => any = () => {
    return (
      <ClickAwayListener onClickAway={hideToast}>
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          open={show}
          TransitionComponent={Fade}
          autoHideDuration={6000}
          onClose={hideToast}
        >
          <Alert onClose={hideToast} severity={severity || toastState.severity}>
            {message || toastState.message}
          </Alert>
        </Snackbar>
      </ClickAwayListener>
    );
  };

  return show ? ReactDOM.createPortal(toastTemplate(), wrapperElement) : <></>;
};
