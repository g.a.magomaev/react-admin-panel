import {
    createStyles,
    Input,
    InputAdornment,
    makeStyles,
    Theme,
    withStyles,
}                 from "@material-ui/core";
import IconButton from '@material-ui/core/IconButton';
import CloseIcon  from '@material-ui/icons/Close';

import SearchIcon                from '@material-ui/icons/Search';
import { useFormik }             from "formik";
import React                     from "react";
import { HTML_INPUT_TYPE }       from "../../enums/html-input-type.enum";
import { TSearchComponentProps } from "./types/search-component-props.type";


const SearchComponent: ( { emitter, style }: TSearchComponentProps ) => JSX.Element = ( { emitter, style }: TSearchComponentProps ) => {

    const { getFieldProps, values, resetForm } = useFormik( {
            initialValues: { value: '' },
            onSubmit:      ( { value } ) => {
                emitter( value );
            },
        },
    );

    const cleanForm: () => void = () => {
        resetForm();
        emitter( '' );
    };

    const useStyles = makeStyles( () => ( {
        searchBtn: {
            width:  '32px',
            height: '32px',
        },
    } ) );

    const styles = useStyles();

    return (
        <SearchField
            fullWidth
            type = { HTML_INPUT_TYPE.TEXT }
            placeholder = 'Search'
            { ...getFieldProps( 'value' ) }
            style = { { ...style } }

            endAdornment = {
                <InputAdornment position = "end">{
                    values.value ?
                        <>
                            <IconButton
                                className = { styles.searchBtn }
                                onClick = { () => emitter( values.value ) }
                            >
                                <SearchIcon/>
                            </IconButton>

                            <IconButton
                                className = { styles.searchBtn }
                                onClick = { cleanForm }
                            >
                                <CloseIcon/>
                            </IconButton>
                        </>

                        :
                        <></> }
                </InputAdornment>
            }


        />
    );
};

const SearchField = withStyles( ( theme: Theme ) =>
    createStyles( {
        root: {
            height:          '32px',
            borderRadius:    4,
            position:        'relative',
            backgroundColor: theme.palette.common.white,
            border:          '1px solid #ced4da',
            fontSize:        16,
            width:           'auto',
            padding:         '0 0 0 10px',
            transition:      theme.transitions.create( [ 'border-color' ] ),

            '&:hover, &:focus, &:active': {
                borderColor: 'blue',
            },

            '&::before, &::after': {
                content: 'none',
            },

        },
    } ),
)( Input );

export default SearchComponent;
