export type TSearchComponentProps = {
    emitter: ( value: string ) => void,
    style?: { [key: string]: string }
}
