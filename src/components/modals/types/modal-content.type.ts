import { TModalFooterProps } from "../../modal/modal-footer/types/modal-footer-props.type";
import { TModalTypes }       from "./modals-types.type";


export type TModalContent = {
    [key: string]: {
        modalType: TModalTypes;
        title?: string;
        footer: TModalFooterProps;
    }
}
