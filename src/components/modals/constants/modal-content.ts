import { BUTTON_VARIANT } from "../../../enums/button-variant.enum";
import {
    BUTTON_TYPE_STYLE,
    MODAL_FOOTER_BTN_POSITION,
    MODAL_TYPE,
}                         from "../../../interfaces/constants";
import { HIDE_MODAL }     from "../../../redux/components/modal/modal.actions";
import {
    CREATE_DEVICE,
    DELETE_DEVICE,
    UPDATE_DEVICE,
}                         from "../../../redux/devices-page/devices-page.actions";
import { ICON_TYPES }     from "../../icon/enums/icon-types.enum";
import { TModalContent }  from "../types/modal-content.type";


const ModalContent: TModalContent = {
    createDeviceModal: {
        modalType: MODAL_TYPE.CREATE_DEVICE_MODAL,
        title:     "Create device",
        footer:    {
            buttonsProps:    [
                {
                    color:       BUTTON_TYPE_STYLE.PRIMARY,
                    variant:     BUTTON_VARIANT.CONTAINED,
                    name:        "Create",
                    style:       { width: "100px" },
                    action:      CREATE_DEVICE,
                    canDisabled: true,
                },
                {
                    color:  BUTTON_TYPE_STYLE.PRIMARY,
                    name:   "Cancel",
                    style:  { width: "100px" },
                    action: HIDE_MODAL,
                },
            ],
            buttonsPosition: MODAL_FOOTER_BTN_POSITION.AROUND,
        },
    },
    editDeviceModal:   {
        modalType: MODAL_TYPE.EDIT_DEVICE_MODAL,
        footer:    {
            buttonsProps:    [
                {
                    color:     BUTTON_TYPE_STYLE.SECONDARY,
                    startIcon: ICON_TYPES.DELETE_FOR_EVER_ICON,
                    action:    DELETE_DEVICE,
                    style:     { width: "32px" },
                },
                {
                    color:       BUTTON_TYPE_STYLE.PRIMARY,
                    variant:     BUTTON_VARIANT.CONTAINED,
                    name:        "Edit",
                    action:      UPDATE_DEVICE,
                    style:       { flexGrow: "1" },
                    canDisabled: true,
                },
                {
                    color:  BUTTON_TYPE_STYLE.PRIMARY,
                    name:   "Cancel",
                    action: HIDE_MODAL,
                    style:  { flexGrow: "1" },
                },
            ],
            buttonsPosition: MODAL_FOOTER_BTN_POSITION.AROUND,
        },
    },

};

export default ModalContent;
