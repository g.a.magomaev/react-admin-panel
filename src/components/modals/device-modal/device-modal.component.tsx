import { TextField }                from "@material-ui/core";
import { useFormik }                from "formik";
import React, { FunctionComponent } from "react";
import { useDispatch }              from "react-redux";
import { Dispatch }                 from "redux";
import styled                       from "styled-components/macro";
import * as Yup                     from "yup";
import { HTML_INPUT_TYPE }          from "../../../enums/html-input-type.enum";
import { TButtonProps }      from "../../../types/button-props.type";
import { TDeviceModalProps } from "./types/device-modal-props.type";
import { IDevice }           from "../../../pages/devices/types/device.type";
import { TReduxAction } from "../../../redux/types/redux-action.interface";
import { TThemeColor }  from "../../../hooks/theme/types/theme-colors.type";
import { ModalFooter }  from "../../modal/modal-footer/modal-footer.component";
import { TModalFooterProps }        from "../../modal/modal-footer/types/modal-footer-props.type";
import ModalContent                 from "../constants/modal-content";


export const DeviceModal: FunctionComponent<TDeviceModalProps> = ( {
    device,
    modalType,
}: TDeviceModalProps ) => {
    const dispatch: Dispatch<TReduxAction<IDevice>> = useDispatch();
    const { id = '', room_id = '', name = '', type = '', description = '' } = device || {};
    const { getFieldProps, touched, errors, isValid: valid, handleSubmit, dirty, values } = useFormik( {
            initialValues:    { id, room_id, name, type, description },
            validationSchema: Yup.object( {
                id:          Yup.number(),
                room_id:     Yup.number().typeError( 'Room id must be a number' ).positive( 'Room id must be more than 0' ).required( "Это поле не должно быть пустым" ),
                name:        Yup.string().trim().required( "Это поле не должно быть пустым" ),
                type:        Yup.string().trim().required( "Это поле не должно быть пустым" ),
                description: Yup.string().trim(),
            } ),
            onSubmit:         () => {
            },
        },
    );
    const footerAction = ( action: any ) => {
        dispatch( action( values ) );
        dispatch( action( values ) );
    };
    const getModalFooterProps: ( data: TModalFooterProps ) => TModalFooterProps =
        ( data: TModalFooterProps ) => {
            const updatedFooterProps: TModalFooterProps = { ...data };
            updatedFooterProps.buttonsProps = data.buttonsProps.map( ( buttonProps: TButtonProps ) => {
                buttonProps.onClickFn = footerAction;
                return buttonProps;
            } );
            return updatedFooterProps;
        };
    return ( <>
            <ModalForm onSubmit = { handleSubmit }>
                { device?.id && (
                    <TextField
                        fullWidth
                        type = { HTML_INPUT_TYPE.TEXT }
                        { ...getFieldProps( 'id' ) }
                        label = 'Device id'
                        disabled = { true }
                    />
                )
                }
                <TextField
                    fullWidth
                    type = { HTML_INPUT_TYPE.TEXT }
                    className = 'input-field'
                    label = 'Room id'
                    placeholder = 'Enter a room id'
                    { ...getFieldProps( 'room_id' ) }
                    error = { touched.room_id && Boolean( errors.room_id ) }
                    helperText = { touched.room_id && errors.room_id }
                    InputLabelProps = { {
                        shrink: true,
                    } }
                />
                <TextField
                    fullWidth
                    type = { HTML_INPUT_TYPE.TEXT }
                    className = 'input-field'
                    label = 'Name'
                    placeholder = 'Enter a device name'
                    { ...getFieldProps( 'name' ) }
                    error = { touched.name && Boolean( errors.name ) }
                    helperText = { touched.name && errors.name }
                    InputLabelProps = { {
                        shrink: true,
                    } }
                />
                <TextField
                    fullWidth
                    type = { HTML_INPUT_TYPE.TEXT }
                    className = 'input-field'
                    label = 'Type'
                    placeholder = 'Enter a type'
                    { ...getFieldProps( 'type' ) }
                    error = { touched.type && Boolean( errors.type ) }
                    helperText = { touched.type && errors.type }
                    InputLabelProps = { {
                        shrink: true,
                    } }
                />
                <TextField
                    fullWidth
                    type = { HTML_INPUT_TYPE.TEXT }
                    className = 'input-field'
                    label = 'Description'
                    placeholder = 'Enter a description'
                    { ...getFieldProps( 'description' ) }
                    error = { touched.description && Boolean( errors.description ) }
                    helperText = { touched.description && errors.description }
                    InputLabelProps = { {
                        shrink: true,
                    } }
                />
            </ModalForm>
            { modalType ?
                <ModalFooter { ...getModalFooterProps( ModalContent[modalType].footer ) } disabledBtn = { !valid || !dirty }/> : null }
        </>
    );
};
const ModalForm = styled.form`
	display          : flex;
	flex-direction   : column;
	width            : 400px;
	padding          : 12px 24px 24px 24px;
	background-color : ${ ( { theme }: { theme: TThemeColor } ) => theme.light01 };

	& > *:not(:last-child) {
		margin-bottom : 4px;
	}

	.input-field {
		height : 72px;
	}
`;
