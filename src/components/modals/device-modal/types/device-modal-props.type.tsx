import { TModalTypes } from "../../types/modals-types.type";
import { IDevice }     from "../../../../pages/devices/types/device.type";


export type TDeviceModalProps = {
    device?: IDevice | null;
    modalType?: TModalTypes;
};
