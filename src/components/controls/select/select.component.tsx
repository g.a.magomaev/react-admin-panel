import React, { ChangeEvent, FunctionComponent, useEffect, useState } from "react";
import styled from 'styled-components';
import { SelectComponentProps } from './types/select-component-props.type';
import { SelectOption } from './types/select-option.type';
import { SelectValue } from './types/select-value.type';

export const SelectComponent: FunctionComponent<SelectComponentProps> = ({ options, onChange, placeholder = '' }: SelectComponentProps) => {
    const [value, setValue] = useState<SelectValue>('');
    const [placeholderColor, setPlaceholderColor] = useState<string>('#000');

    useEffect(() => {
        setValue(placeholder);
        setPlaceholderColor(placeholder ? '#8f8f8f' : '#000');
    }, [placeholder])

    const handleChange = ({ target: { value } }: ChangeEvent<HTMLSelectElement>) => {
        onChange(value);
        setValue(value);
        setPlaceholderColor('#000');
    };

    const getOptions = (options: SelectOption[]) => {
        let newOptions = options.map(({ value, label }: SelectOption) => (
            <Option key={value} value={value}>{label}</Option>
        ));

        if (placeholder) {
            newOptions.unshift(<Option hidden key={placeholder} value=''>{placeholder}</Option>)
        }

        return newOptions;
    }

    return (
        <Select placeholderColor={placeholderColor} value={value} onChange={handleChange}>
            {getOptions(options)}
        </Select>
    )
}

const Select = styled.select<{ placeholderColor: string }>`
    color: ${({ placeholderColor }) => placeholderColor};
`;

const Option = styled.option`
    color: #000;
`;

