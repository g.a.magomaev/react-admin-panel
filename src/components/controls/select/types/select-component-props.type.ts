import { SelectOption } from './select-option.type';
import { SelectValue } from './select-value.type';

export type SelectComponentProps = {
    options: SelectOption[];
    onChange: (value: SelectValue) => SelectValue;
    placeholder?: string;
}