import {
    Paper,
    Theme,
    withStyles,
}                          from "@material-ui/core";
import ClickAwayListener   from '@material-ui/core/ClickAwayListener';
import React, {
    FunctionComponent,
    useEffect,
}                          from "react";
import { createPortal }    from "react-dom";
import {
    useDispatch,
    useSelector,
}                          from "react-redux";
import { TAppState }       from "../../redux/appState";
import { HIDE_DROPDOWN }   from "../../redux/components/dropdown/dropdown.actions";
import { TContainerProps } from "./types/container-dropdown-props.type";


const DropDownComponent: FunctionComponent = () => {
    const dispatch = useDispatch();
    const wrapperElement: HTMLDivElement = document.createElement( "div" );
    const { element, show, currentTarget } =
        useSelector( ( state: TAppState ) => state.dropdown );

    const getDropdownContent: () => JSX.Element = () => {
        let { offsetTop, offsetLeft, offsetWidth, offsetHeight }: any = currentTarget;

        return (
            <ClickAwayListener onClickAway = { handleClose }>

                <Container elevation = { 3 }
                           square
                           left = { offsetLeft }
                           top = { offsetTop + offsetHeight }
                           width = { offsetWidth }
                >
                    { element }
                </Container>
            </ClickAwayListener>
        );
    };

    useEffect( () => {
        const dropdownRoot: HTMLElement = document.getElementById( "dropdown-root" )!;

        dropdownRoot.append( wrapperElement );
        return () => {
            dropdownRoot.removeChild( wrapperElement );
        };
    }, [ show, wrapperElement ] );

    const handleClose = () => {
        dispatch( HIDE_DROPDOWN() );
    };

    return ( show && currentTarget && element ? createPortal( getDropdownContent(), wrapperElement ) : null );
};

const Container = withStyles<"root", {}, TContainerProps>( ( { palette: { common } }: Theme ) => ( {
    root: {
        position:        'absolute',
        top:             ( { top }: TContainerProps ) => top,
        left:            ( { left }: TContainerProps ) => left,
        width:           ( { width }: TContainerProps ) => width,
        display:         'flex',
        alignItems:      'center',
        justifyContent:  'center',
        zIndex:          1600,
        backgroundColor: common.white,
    },

} ) )( Paper );

export default DropDownComponent;

/*
 Use with redux: dispatch( SHOW_DROPDOWN( { element: (<div>some component</div>), currentTarget: e.currentTarget } ) ).
 'currentTarget' - it is currentTarget from onClick callback function event, after click on some element.
 */
