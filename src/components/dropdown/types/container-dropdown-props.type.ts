export type TContainerProps = {
    top: number;
    left: number;
    width: number;
}
