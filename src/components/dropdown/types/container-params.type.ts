export type TContainerParams = {
    offsetTop: number,
    offsetLeft: number,
    offsetWidth: number,
    offsetHeight: number,
}
