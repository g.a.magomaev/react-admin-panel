export type TDropdownProps = {
    currentTarget: EventTarget
    element: JSX.Element
}
