export type TFilterConfig = {
    label: string,
    filterBy: string,
}
