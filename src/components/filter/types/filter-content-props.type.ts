import { IObjectKeys } from "../../../interfaces/objectKeys.interface";
import { TFilterConfig } from "./filter-item.type";


export type TFilterContentProps = {
    items: IObjectKeys[],
    configs: TFilterConfig[],
    getValue: ( value: any ) => void;
}
