import { IObjectKeys }   from "../../../interfaces/objectKeys.interface";
import { TFilterConfig } from "./filter-item.type";


export type TFilterProps = {
    checkedAll?: boolean, // note: if 'true' a checkbox will be visible.
    filterConfig: TFilterConfig[],
    items: IObjectKeys[];
    getValue: ( value: any ) => void;
}
