import React           from "react";
import { IObjectKeys } from "../../../interfaces/objectKeys.interface";


export type TFilterListItemProps = {
    filterBy: string,
    item: IObjectKeys,
    setFormValue: ( itemEvent: React.ChangeEvent<HTMLInputElement>, filterBy: string ) => void,
    form: IObjectKeys
}
