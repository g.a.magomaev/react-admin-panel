import { IObjectKeys } from "../../../interfaces/objectKeys.interface";


export type TFilterFooterProps = {
    filterValue: IObjectKeys,
    emmitFilterValue: ( value: IObjectKeys ) => void
}
