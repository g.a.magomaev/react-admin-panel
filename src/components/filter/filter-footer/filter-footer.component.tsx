import {
    Box,
    Button,
    Theme,
    withStyles,
}                                   from "@material-ui/core";
import React, { FunctionComponent } from "react";
import { TFilterFooterProps }       from "../types/filter-footer-props.type";


const FilterFooterComponent: FunctionComponent<TFilterFooterProps> = ( { filterValue, emmitFilterValue } ) => (
    <Container>
        <Button
            color = "primary"
            variant = "contained"
            fullWidth = { true }
            type = "button"
            onClick = { () => emmitFilterValue( filterValue ) }
        >
            Filter
        </Button>
    </Container>
);

const Container = withStyles( ( { palette, spacing }: Theme ) => ( {
    root: {
        display:         'flex',
        flexDirection:   'column',
        width:           '100%',
        position:        'sticky',
        bottom:          '1px',
        justifyContent:  'center',
        padding:         spacing( 1 ),
        backgroundColor: palette.common.white,
        marginTop:       'auto',
    },
} ) )( Box );

export default FilterFooterComponent;
