import {
    Box,
    Theme,
    withStyles,
} from "@material-ui/core";
import React, {
    FunctionComponent,
    useCallback,
    useEffect,
    useState,
} from "react";

import { IObjectKeys }         from "../../../interfaces/objectKeys.interface";
import FilterFooterComponent   from "../filter-footer/filter-footer.component";
import FilterListItemComponent from "../filter-list-item/filter-list-item.component";
import FilterListComponent     from "../filter-list/filter-list.component";
import { TFilterContentProps } from "../types/filter-content-props.type";
import { TFilterConfig }       from "../types/filter-item.type";


const FilterContentComponent: FunctionComponent<TFilterContentProps> = ( { configs, items, getValue } ) => {

    const [ form, setForm ] = useState<IObjectKeys>( {} );

    const setFormValue = ( { target: { name, checked } }: React.ChangeEvent<HTMLInputElement>, filterBy: string ) => {
        setForm( ( form ) => ( { ...form, [filterBy]: { ...form[filterBy], [name]: checked } } ) );
    };

    const initForm = useCallback(
        ( { configs, items } ) => {
            let newForm: IObjectKeys = {};
            configs.forEach( ( config: TFilterConfig ) => {
                newForm[config.filterBy] = {};
                items.forEach( ( item: IObjectKeys ) => {
                    newForm[config.filterBy][item.id] = false;
                } );
            } );
            return newForm;
        }, [],
    );

    useEffect( () => {
        setForm( initForm( { configs, items } ) );
    }, [ items, configs, initForm ] );

    const emmitFormValue = () => {
        getValue( getOutputData() );
    };

    const getOutputData: () => IObjectKeys = () => {
        const value: IObjectKeys = {};
        Object.keys( form ).forEach( ( filterBy: string ) => {
            value[filterBy] = [];
            items.forEach( ( item: IObjectKeys ) => {
                if ( form[filterBy][item.id] ) {
                    value[filterBy].push( item.type );
                }
            } );
            if ( !value[filterBy].length ) {
                delete value[filterBy];
            }
        } );

        return value;
    };

    return (
        <Content>
            <Lists>
                { configs.map( ( { filterBy, label }: TFilterConfig ) => (
                        <FilterListComponent key = { filterBy } { ...{ label } }>
                            { items.map( ( item: IObjectKeys ) => (
                                <FilterListItemComponent key = { item.id } { ...{ item, setFormValue, filterBy, form } }/>
                            ) ) }
                        </FilterListComponent>
                    ),
                ) }
            </Lists>
            <FilterFooterComponent filterValue = { form }
                                   emmitFilterValue = { emmitFormValue }
            />
        </Content>
    );
};

const Content = withStyles( {
    root: {
        display:       'flex',
        flexDirection: 'column',
        width:         '100%',
    },
} )( Box );

const Lists = withStyles( ( { spacing }: Theme ) => ( {
    root: {
        display:       'flex',
        flexDirection: 'column',
        width:         '100%',
        left:          spacing( 0 ),
        maxHeight:     spacing( 35.5 ),
        overflowY:     'auto',
    },
} ) )( Box );

export default FilterContentComponent;
