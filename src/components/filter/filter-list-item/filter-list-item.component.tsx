import {
    Checkbox,
    FormControlLabel,
    ListItem,
    Theme,
    withStyles,
}                                   from "@material-ui/core";
import React, { FunctionComponent } from "react";
import { TFilterListItemProps }     from "../types/filter-list-item-props.type";


const FilterListItemComponent: FunctionComponent<TFilterListItemProps> =
    ( { item, filterBy, setFormValue, form } ) =>
        <Container
            key = { item.id }
            button
        >
            <FormControlLabel control = {
                <Checkbox name = { item.id.toString() }
                          onChange = { ( e: React.ChangeEvent<HTMLInputElement> ) =>
                              setFormValue( e, filterBy ) }
                          size = { "small" }
                />
            }
                              label = { item[filterBy] }
                              checked = { form[filterBy] ? form[filterBy][item.id.toString()] : false }

            />
        </Container>
;

const Container = withStyles( ( { spacing }: Theme ) => ( {
    root: {
        padding: spacing( 0 ),

        '&>label': {
            width:  '100%',
            margin: '0',

            '&>span': {
                padding: spacing( 0.25, 0.25, 0.25, 1 ),
            },
        },
    },
} ) )( ListItem );

export default FilterListItemComponent;
