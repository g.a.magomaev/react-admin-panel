import {
    Box,
    List,
    Theme,
    withStyles,
}                                   from "@material-ui/core";
import React, { FunctionComponent } from "react";
import { TFilterListProps }         from "../types/filter-list-props.type";


const FilterListComponent: FunctionComponent<TFilterListProps> =
    ( { label, children } ) => (
        <List>
            <ListTitle>{ label }</ListTitle>
            { children }
        </List>
    );

const ListTitle = withStyles( ( { palette, spacing }: Theme ) => ( {
    root: {
        position:        'sticky',
        top:             '-1px',
        zIndex:          1601,
        padding:         spacing( 0.5, 1 ),
        marginBottom:    spacing( 0.5 ),
        backgroundColor: palette.common.white,
        boxShadow:       `inset 0 -1px 0 0 ${ palette.grey.A100 }`,
    },
} ) )( Box );

export default FilterListComponent;
