import { IObjectKeys } from "../../../interfaces/objectKeys.interface";


export interface IFilterOutput extends IObjectKeys {
    [key: string]: string[];
}
