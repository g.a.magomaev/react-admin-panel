import {
    Box,
    Theme,
    withStyles,
}                             from "@material-ui/core";
import React, {
    FunctionComponent,
    useCallback,
    useEffect,
    useState,
}                             from 'react';
import { useDispatch }        from "react-redux";
import { SHOW_DROPDOWN }      from "../../redux/components/dropdown/dropdown.actions";
import FilterContentComponent from "./filter-content/filter-content.component";
import { TFilterConfig }      from "./types/filter-item.type";
import { TFilterProps }       from "./types/filter-props.type";


export const Filter: FunctionComponent<TFilterProps> = ( {
    filterConfig: configs,
    items,
    getValue,

}: TFilterProps ) => {
    const dispatch = useDispatch();
    const [ filterState, setFilterState ] = useState<JSX.Element>( <></> );

    const openDropdown = ( { currentTarget }: React.MouseEvent<HTMLDivElement> ) => {
        dispatch( SHOW_DROPDOWN( { element: ( filterState ), currentTarget } ) );
    };

    const getFilterContent = useCallback( ( items: any[], configs: TFilterConfig[] ) =>
            <FilterContentComponent { ...{ configs, items, getValue } }/>
        , [ getValue ] );

    useEffect( () => {
        setFilterState( getFilterContent( items, configs ) );
    }, [ items, configs, getFilterContent ] );

    return (
        <Container
            onClick = { ( e: React.MouseEvent<HTMLDivElement> ) => openDropdown( e ) }
        >
            Filter
        </Container>
    );
};

const Container = withStyles( ( { palette, spacing }: Theme ) => ( {
    root: {
        display:      'flex',
        padding:      spacing( 0, 1 ),
        alignItems:   'center',
        border:       `1px solid ${ palette.grey.A100 }`,
        borderRadius: spacing( 0.5 ),
        cursor:       'pointer',
    },
} ) )( Box );
