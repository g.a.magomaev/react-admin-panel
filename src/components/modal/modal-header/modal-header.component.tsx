import CloseIcon        from "@material-ui/icons/Close";
import React, {
    Dispatch,
    FunctionComponent,
}                       from "react";
import { useDispatch }  from "react-redux";
import styled           from "styled-components/macro";
import { HIDE_MODAL }   from "../../../redux/components/modal/modal.actions";
import { TReduxAction } from "../../../redux/types/redux-action.interface";
import { TThemeColor }  from "../../../hooks/theme/types/theme-colors.type";


type TModalHeaderProps = {
    title?: string;
};

export const ModalHeader: FunctionComponent<TModalHeaderProps> = ({
    title,
}: TModalHeaderProps) => {
    const dispatch: Dispatch<TReduxAction<void>> = useDispatch();

    const handleClose = () => {
        dispatch(HIDE_MODAL());
    };

    return (
        <Container>
            <Title>{ title }</Title>
            <CloseBtn onClick = { handleClose }>
                <CloseIcon/>
            </CloseBtn>
        </Container>
    );
};

const Container = styled.div`
	display: flex;
	flex-direction: row;
	width: 100%;
	padding: 12px 12px 12px 24px;
	background-color: ${ ({ theme: { light03 } }: { theme: TThemeColor }) => light03 };
`;
const Title     = styled.div`
	display: flex;
	flex-direction: row;
	width: 100%;
	font-size: 22px;
`;
const CloseBtn  = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;

	svg {
		width: 20px;
		height: 20px;
		cursor: pointer;
	}
`;
