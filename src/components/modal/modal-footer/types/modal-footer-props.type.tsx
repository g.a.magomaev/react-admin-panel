import { TButtonProps } from "../../../../types/button-props.type";


export type TModalFooterProps = {
    buttonsProps: TButtonProps[];
    buttonsPosition?: string;
    disabledBtn?: boolean;
};
