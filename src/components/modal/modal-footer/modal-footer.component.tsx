import {
    Button,
    IconButton,
}                                    from "@material-ui/core";
import React, { FunctionComponent }  from "react";
import styled                        from "styled-components/macro";
import { MODAL_FOOTER_BTN_POSITION } from "../../../interfaces/constants";
import { TButtonProps }              from "../../../types/button-props.type";
import IconComponent                 from "../../icon/icon.component";
import { TModalFooterProps }         from "./types/modal-footer-props.type";


export const ModalFooter: FunctionComponent<TModalFooterProps> = ({
    buttonsProps,
    buttonsPosition = MODAL_FOOTER_BTN_POSITION.CENTER,
    disabledBtn,
}: TModalFooterProps) => {

    return (
        <Container position = { buttonsPosition }>
            { buttonsProps.map((buttonProps: TButtonProps, buttonIndex: number) => {
                    const {
                              name,
                              style,
                              onClickFn,
                              action,
                              color,
                              endIcon   = '',
                              startIcon = '',
                              variant,
                              canDisabled,
                          }
                              = buttonProps;
                    return !name ?
                        <IconButton
                            key = { buttonIndex }
                            { ...{
                                color,
                                variant,
                            } }
                            onClick = { () => onClickFn ? onClickFn(action) : null }
                            { ...{ style } }
                        >
                            { startIcon ? <IconComponent iconType = { startIcon }/> : null }
                        </IconButton> :
                        <Button
                            key = { buttonIndex }
                            { ...{
                                color,
                                variant,
                            } }
                            disabled = { canDisabled ? disabledBtn : false }
                            endIcon = { endIcon ? <IconComponent iconType = { endIcon }/> : null }
                            startIcon = { startIcon ? <IconComponent iconType = { startIcon }/> : null }
                            onClick = { () => onClickFn ? onClickFn(action) : null }
                            { ...{ style } }
                        >
                            { name }
                        </Button>
                },
            ) }
        </Container>
    );
};
const Container                                                = styled.div<{ position: string }>`
	display: flex;
	flex-direction: row;
	justify-content: ${ ({ position }: { position: string }) => position };
	width: 100%;
	padding: 0 24px 24px 24px;

	button {
		height: 32px;
		margin: 0 4px;
	}
`;
