import React, {
    Dispatch,
    FunctionComponent,
    ReactNode,
    useEffect,
    useState,
}                       from "react";
import { useDispatch }  from "react-redux";
import styled           from "styled-components";
import { TThemeColor }  from "../../../hooks/theme/types/theme-colors.type";
import { HIDE_MODAL }   from "../../../redux/components/modal/modal.actions";
import { TReduxAction } from "../../../redux/types/redux-action.interface";


export const ModalOverlay: FunctionComponent<{ children: ReactNode, show: boolean }> = ( {
    children,
    show,
}: {
    children: ReactNode;
    show: boolean
} ) => {
    const dispatch: Dispatch<TReduxAction<void>> = useDispatch();
    const [ showModal, setShowModal ] = useState( false );

    const handleClose: () => void = () => {
        let timeout;

        if ( timeout ) {
            clearTimeout( timeout );
        }

        setShowModal( false );

        timeout = setTimeout( () => {
            dispatch( HIDE_MODAL() );
        }, 200 );
    };

    useEffect( () => {
        const timeout = setTimeout( () => {
            setShowModal( show );
        }, 200 );

        return () => clearTimeout( timeout );
    }, [ show ] );

    return <Overlay opacity = { showModal ? 1 : 0 }
                    onClick = { handleClose }
    >{ children }</Overlay>;
};

const Overlay = styled.div<{ opacity: number }>`
	display          : flex;
	align-items      : center;
	justify-content  : center;
	position         : fixed;
	top              : 0;
	left             : 0;
	width            : 100vw;
	height           : 100vh;
	background-color : ${ ( { theme: { labelQuaternary } }: { theme: TThemeColor } ) => labelQuaternary };
	opacity          : ${ ( { opacity } ) => opacity };
	transition       : opacity 0.2s linear;
`;
