import React, {
    FunctionComponent,
    useEffect,
}                       from "react";
import ReactDOM         from "react-dom";
import { useSelector }  from "react-redux";
import styled           from "styled-components/macro";
import { TThemeColor }  from "../../hooks/theme/types/theme-colors.type";
import { MODAL_TYPE }   from "../../interfaces/constants";
import { IDevice }      from "../../pages/devices/types/device.type";
import { TAppState }    from "../../redux/appState";
import { TModalState }  from "../../redux/components/modal/types/modal-state.type";
import ModalContent     from "../modals/constants/modal-content";
import { DeviceModal }  from "../modals/device-modal/device-modal.component";
import { TModalTypes }  from "../modals/types/modals-types.type";
import { ModalHeader }  from "./modal-header/modal-header.component";
import { ModalOverlay } from "./modal-overlay/modal-overlay";


export const MainModal: FunctionComponent = () => {
    const wrapperElement: HTMLDivElement = document.createElement( "div" );
    const { type, content, show }: TModalState = useSelector( ( state: TAppState ) => state.modal );

    useEffect( () => {
        const modalRoot: HTMLElement = document.getElementById( "modal-root" )!;
        modalRoot.append( wrapperElement );
        return () => {
            modalRoot.removeChild( wrapperElement );
        };
    }, [ show, wrapperElement ] );

    const getTemplateModalContainer: () => JSX.Element = () => {
        return (
            <ModalOverlay show = { show }>
                <ModalContainer onClick = { ( e ) => e.stopPropagation() }>
                    { type && getModalTemplateContent( type, content ) }
                </ModalContainer>
            </ModalOverlay>
        );
    };

    const getModalTemplateContent = ( type: TModalTypes, device: IDevice | null ) => {
        switch ( type ) {
            case MODAL_TYPE.CREATE_DEVICE_MODAL:
            case MODAL_TYPE.EDIT_DEVICE_MODAL:
                return (
                    <>
                        <ModalHeader
                            title = { device ? device.name : ModalContent[type].title }
                        />
                        <DeviceModal modalType = { type } { ...{ device } }/>
                    </>
                );
            default:
                return null;
        }
    };
    return show ? (
        ReactDOM.createPortal( getTemplateModalContainer(), wrapperElement )
    ) : (
        <></>
    );
};
const ModalContainer = styled.div`
	background-color : ${ ( { theme: { light01 } }: { theme: TThemeColor } ) => light01 };
`;
export type TModalProps = {
    show: boolean;
    type: string;
    content?: any;
};
