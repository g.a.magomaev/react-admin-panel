import {
    Box,
    createStyles,
    makeStyles,
    Tab,
    Tabs,
    Theme,
}                               from '@material-ui/core';
import React, {
    ChangeEvent,
    FunctionComponent,
    memo,
    useState,
}                               from "react";
import { Link }                 from "react-router-dom";
import { TRouterLink }          from "../../types/router-link.type";
import { THorizontalTabsProps } from "./horizontal-tabs-props.type";


const HorizontalTabsComponent: FunctionComponent<THorizontalTabsProps> = memo( ( { links } ) => {
    const { tab, container } = useStyles();
    const [ value, setValue ] = useState<number>( 1 );

    const handleChange = ( event: ChangeEvent<{}>, newValue: number ) => {
        if ( value === newValue ) {
            return;
        }
        setValue( newValue );
    };

    return (
        <Box>
            <Tabs
                className = { container }
                value = { value }
                indicatorColor = "primary"
                textColor = "primary"
                onChange = { handleChange }
                selectionFollowsFocus = { true }
            >
                { links?.length && links.map( ( { id, linkName, path }: TRouterLink ) =>
                    <Tab
                        key = { id }
                        value = { id }
                        label = { linkName }
                        className = { value !== id ? tab : '' }
                        component = { Link }
                        to = { path }
                    />,
                ) }
            </Tabs>
        </Box>
    );
} );

const useStyles = makeStyles( ( { palette: { grey } }: Theme ) =>
    createStyles( {
        container: {
            boxShadow: `inset 0 -2px 0 0 ${ grey.A100 }`,
        },
        tab:       {
            '&:hover': {
                color:      grey["800"],
                transition: 'color 0.3s linear',
            },
        },
    } ),
);

export default HorizontalTabsComponent;
