import { TRouterLink } from "../../types/router-link.type";


export type THorizontalTabsProps = {
    links: TRouterLink[],
}
