export type TIconTypes =
    'DeleteForeverIcon'
    | 'Settings'
    | 'Home'
    | 'Devices'
    | 'ExitToApp';
