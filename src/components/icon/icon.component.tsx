import {
    Devices,
    ExitToApp,
    Home,
    Settings,
}                        from "@material-ui/icons";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import React             from "react";
import { TIconTypes }    from "./types/icon-types.type";


const IconComponent =
    ( { iconType }: { iconType: TIconTypes } ) => {
        const getIcon = () => {
            switch ( iconType ) {
                case "DeleteForeverIcon":
                    return <DeleteForeverIcon/>;

                case "Settings":
                    return <Settings/>;
                case "Devices":
                    return <Devices/>;
                case "Home":
                    return <Home/>;
                case "ExitToApp":
                    return <ExitToApp/>;

                default:
                    return null;
            }
        };

        return (
            getIcon()
        );
    };

export default IconComponent;
