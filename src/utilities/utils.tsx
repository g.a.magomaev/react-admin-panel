import { BASE_URL }            from "../constants/base-url.constant";
import { HTTP_REQUEST_METHOD } from "../interfaces/constants";
import { TRequestProps }       from "../types/request-props.type";
import { TResponseMessage }    from '../types/response-message.type';


export class Utils {
    static httpErrorRecognition ( errorStatus: number ) {
        let errorType: string;

        switch ( errorStatus ) {
            case 500:
                errorType = "error";
                break;

            case 400:
                errorType = "warning";
                break;

            case 200:
            case 201:
                errorType = "success";
                break;

            default:
                errorType = "";
                break;
        }

        return errorType;
    }

    // ----- FETCH HELPERS ----- //

    static getFetch ( {
        url,
        method = HTTP_REQUEST_METHOD.GET,
        body = null,
        headers = {},
        searchQuery = "",
        queryType = "",
        sortQuery,
    }: TRequestProps ): Promise<TResponseMessage> {
        if ( body ) {
            body = JSON.stringify( body );
            headers["Content-Type"] = "application/json";
        }

        let fullUrl: string = BASE_URL + url;

        if ( searchQuery ) {
            const newUrl = new URL( fullUrl );
            newUrl.searchParams.append( queryType, searchQuery );

            fullUrl = newUrl.toString();
        }
        if ( sortQuery?.order && sortQuery?.sortBy ) {
            const newUrl = new URL( fullUrl );
            newUrl.searchParams.append( queryType, JSON.stringify( sortQuery ) );

            fullUrl = newUrl.toString();
        }

        return fetch( fullUrl, {
            method,
            body,
            headers,
        } ).then( ( response: Response ) => response.json() );
    }
}
